package com.app.base.simples.list.tree;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;

import com.app.base.databinding.ActivitySelectableTreeListBinding;
import com.app.base.simples.list.tree.bean.FirstNode;
import com.app.base.simples.list.tree.bean.SecondNode;
import com.app.base.simples.list.tree.bean.ThirdNode;
import com.app.base.simples.list.tree.provider.FirstProvider;
import com.app.base.simples.list.tree.provider.SecondProvider;
import com.app.base.simples.list.tree.provider.ThirdProvider;
import com.chad.library.adapter.base.entity.node.BaseNode;
import com.zndroid.base.binder.AbsProvider;
import com.zndroid.base.simples.SimplePresenter;
import com.zndroid.base.ui.common.CommonNodeListActivity;

import java.util.ArrayList;
import java.util.List;

/**
 * @author lzy
 * @date 2021/5/20
 */
public class SelectableTreeListActivity extends CommonNodeListActivity<ActivitySelectableTreeListBinding, SimplePresenter, DefaultTreeAdapter> {
    @Override
    protected DefaultTreeAdapter onSupplyAdapter() {
        return new DefaultTreeAdapter();
    }

    @Override
    protected RecyclerView onSupplyRecyclerView() {
        //findViewById(R.id.rv);
        return currentViewBinding().rv;
    }

    @Override
    protected AbsProvider<DefaultTreeAdapter>[] onSupplyProviders() {
        return new AbsProvider[]{new FirstProvider(), new SecondProvider(), new ThirdProvider()};
    }

    @Override
    protected ActivitySelectableTreeListBinding onSupplyViewBinding(@NonNull LayoutInflater layoutInflater) {
        //R.layout.activity_selectable_tree_list
        return ActivitySelectableTreeListBinding.inflate(layoutInflater);
    }

    @Override
    protected void doOnCreate(@Nullable Bundle savedInstanceState) {
        super.doOnCreate(savedInstanceState);

        initData();
    }

    private void initData() {
        List<BaseNode> list = new ArrayList<>();
        FirstNode firstNode1 = new FirstNode(0, -1, "0");
        SecondNode secondNode1 = new SecondNode(1, 0, "01");
        SecondNode secondNode2 = new SecondNode(2, 0, "02");
        SecondNode secondNode3 = new SecondNode(3, 0, "03");

        FirstNode firstNode2 = new FirstNode(4, -1, "1");
        SecondNode secondNode4 = new SecondNode(5, 4, "11");
        SecondNode secondNode5 = new SecondNode(6, 4, "12");
        SecondNode secondNode6 = new SecondNode(7, 4, "13");

        ThirdNode thirdNode1 = new ThirdNode(8, 1, "011");
        ThirdNode thirdNode2 = new ThirdNode(9, 6, "121");
        ThirdNode thirdNode3 = new ThirdNode(10, 6, "122");

        ThirdNode thirdNode4 = new ThirdNode(11, -1, "2");

        list.add(firstNode1);
        list.add(secondNode1);
        list.add(secondNode2);
        list.add(secondNode3);
        list.add(firstNode2);
        list.add(secondNode4);
        list.add(secondNode5);
        list.add(secondNode6);
        list.add(thirdNode1);
        list.add(thirdNode2);
        list.add(thirdNode3);
        list.add(thirdNode4);

        currentAdapter().setList(list);
        currentAdapter().toSwitchSelectable(true);
    }

    public void unSelectAll(View view) {
        currentAdapter().toUnSelectAll();
    }

    public void selectAll(View view) {
        currentAdapter().toSelectAll();
    }

    public void toSelect(List<BaseNode> expectList) {
//        currentAdapter().toSelect(expectList);
    }

    public void getSelected(View view) {
        for (BaseNode it : currentAdapter().getSelectedList()) {
            Log.i("hyhy", "===> " + it.toString());
        }
    }
}
