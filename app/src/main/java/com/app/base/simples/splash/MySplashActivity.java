package com.app.base.simples.splash;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ObjectAnimator;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.Nullable;

import com.app.base.R;
import com.app.base.simples.activitys.SecondActivity;
import com.zndroid.base.annotation.ISplashTimerType;
import com.zndroid.base.ui.common.CommonSplashActivity;

/**
 * @author lazy
 * @date 3/15/21
 */
public class MySplashActivity extends CommonSplashActivity {
    @Override
    protected long onSupplyDuration() {
        return 4_000;
    }

    @Override
    protected View onSupplyView() {
        return getLayoutInflater().inflate(R.layout.layout_my_splash, null, false);
    }

    @Override
    protected int onSupplyTimerType() {
        return ISplashTimerType.TYPE_ROUND_RECT;
    }

    @Override
    protected Intent onSupplyMainIntent() {
        return new Intent(this, SecondActivity.class);
    }

    @Override
    protected void onUpdateText(TextView textView, long duration) {
        textView.setText(String.format(getString(R.string.txt_splash_tip), duration));
    }

    @Override
    protected void doSplash() {
        showToast("做一些其他的耗时操作");
        new Handler(this.getMainLooper()).postDelayed(new Runnable() {
            @Override
            public void run() {
                notifyToMain();
            }
        }, 6000);/*模拟耗时操作*/
    }

    @Override
    protected void didSplash() {
        showToast("闪屏结束咯");
    }

    @Override
    protected boolean isSupplySkipSplash() {
        boolean isNotFirstRun = false;
        //other logic
        return isNotFirstRun;
    }

    @Override
    protected boolean isCanBackOnRunning() {
        return false;
    }

    @Override
    protected boolean isSupplyWaitNotify() {
        return false;
    }

    @Override
    protected void doOnCreate(@Nullable Bundle savedInstanceState) {
        super.doOnCreate(savedInstanceState);

        anim();
    }

    @Override
    protected void doOnInitViewAfterAllReady(View rootView) {
        super.doOnInitViewAfterAllReady(rootView);
        logI(rootView.toString());
    }

    private void anim() {
        TextView appName = currentRootView().findViewById(R.id.title);
        ObjectAnimator anim = ObjectAnimator.ofFloat(appName, "alpha", 0, 1);
        anim.setDuration(onSupplyDuration() - 500);
        anim.setRepeatCount(1);
        anim.setRepeatMode(ObjectAnimator.REVERSE);
        anim.start();
        anim.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                appName.setVisibility(View.INVISIBLE);
            }
        });

        appName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                toInterceptJump(true);
            }
        });
    }
}
