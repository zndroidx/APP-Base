package com.app.base.simples.fragment;

import android.Manifest;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.app.base.databinding.LayoutMyFragmentBinding;
import com.jeremyliao.liveeventbus.LiveEventBus;
import com.zndroid.base.simples.SimpleEvent;
import com.zndroid.base.simples.SimplePresenter;
import com.zndroid.base.ui.common.CommonFragment;

import java.util.Collections;
import java.util.List;

/**
 * Created by lzy on 2020/12/3.
 */
public class MyFragment extends CommonFragment<LayoutMyFragmentBinding, SimplePresenter> {

    @Override
    protected LayoutMyFragmentBinding onSupplyViewBinding(@NonNull LayoutInflater layoutInflater, @Nullable ViewGroup parent) {
        //R.layout.layout_my_fragment;
        return LayoutMyFragmentBinding.inflate(layoutInflater, parent, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        SimpleEvent<String> simpleEvent = new SimpleEvent<String>();
        simpleEvent.setData("MyFragment post");
        LiveEventBus.get(SimpleEvent.class).postDelay(simpleEvent, 0);

    }

    @Override
    protected List<String> onSupplyPermissions() {
        return Collections.singletonList(Manifest.permission.CALL_PHONE);
    }

    @Override
    protected void onBackAllPermissionGranted() {
        showToast("onBackAllPermissionGranted");
    }

    @Override
    protected void onBackPermissionDenied(List<String> deniedList) {
        super.onBackPermissionDenied(deniedList);
    }

    @Override
    protected void onBackVisibilityChanged(boolean visible) {
//        showToast(visible + "======");
    }

    @Override
    protected void doOnBackPressed() {
        super.doOnBackPressed();
        Log.i("xxx", "拦截了返回键");
    }

    @Override
    protected boolean isInterceptBackPressed() {
        return false;
    }

    @Override
    protected boolean isTranslucentStatusBar() {
        return true;
    }

    @Override
    protected boolean isValidateStatueBarColorOfFragment() {
        return true;
    }
}
