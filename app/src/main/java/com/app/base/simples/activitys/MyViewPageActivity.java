package com.app.base.simples.activitys;

import android.graphics.Color;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import com.app.base.simples.fragment.DemoFragment;
import com.app.base.simples.fragment.TestFragment;
import com.zndroid.base.simples.SimplePresenter;
import com.zndroid.base.simples.SimpleViewPageActivity;

public class MyViewPageActivity extends SimpleViewPageActivity<SimplePresenter> {
    @Override
    protected Fragment[] onSupplyFragments() {
        TestFragment f0 = new TestFragment();
        Bundle b0 = new Bundle();
        b0.putInt(TestFragment.KEY_COLOR, Color.RED);
        b0.putString(TestFragment.KEY, "Tab 1");
        f0.setArguments(b0);

        TestFragment f1 = new TestFragment();
        Bundle b1 = new Bundle();
        b1.putInt(TestFragment.KEY_COLOR, Color.GREEN);
        b1.putString(TestFragment.KEY, "Tab 2");
        f1.setArguments(b1);

        TestFragment f2 = new TestFragment();
        Bundle b2 = new Bundle();
        b2.putInt(TestFragment.KEY_COLOR, Color.BLUE);
        b2.putString(TestFragment.KEY, "Tab 3");
        f2.setArguments(b2);

        DemoFragment f3 = new DemoFragment();

        return new Fragment[] {
                f0, f1, f2, f3
        };
    }
}
