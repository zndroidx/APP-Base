package com.app.base.simples.ntb;

import android.graphics.Color;
import android.os.Bundle;

import androidx.core.content.res.ResourcesCompat;
import androidx.fragment.app.Fragment;

import com.app.base.R;
import com.app.base.simples.fragment.TestFragment;
import com.zndroid.base.simples.SimpleBottomNavigationTabActivity;
import com.zndroid.base.simples.SimplePresenter;
import com.zndroid.navigation.NavigationTabBarVP2;

public class MainBottomActivity extends SimpleBottomNavigationTabActivity<SimplePresenter> {
    @Override
    protected NavigationTabBarVP2.Model[] onSupplyBars() {
        NavigationTabBarVP2.Model model0 = new NavigationTabBarVP2.Model.Builder(ResourcesCompat.getDrawable(getResources(), R.drawable.ic___news_normal, null), Color.parseColor("#F7F7F7"))
                .title("要闻")
                .selectedIcon(ResourcesCompat.getDrawable(getResources(), R.drawable.ic___news_selected, getTheme()))
                .build();
        NavigationTabBarVP2.Model model1 = new NavigationTabBarVP2.Model.Builder(ResourcesCompat.getDrawable(getResources(), R.drawable.ic___mine_normal, null), Color.parseColor("#F7F7F7"))
                .title("我的")
                .selectedIcon(ResourcesCompat.getDrawable(getResources(), R.drawable.ic___mine_selected, getTheme()))
                .build();
        NavigationTabBarVP2.Model model2 = new NavigationTabBarVP2.Model.Builder(ResourcesCompat.getDrawable(getResources(), R.drawable.ic___setting_normal, null), Color.parseColor("#F7F7F7"))
                .title("设置")
                .selectedIcon(ResourcesCompat.getDrawable(getResources(), R.drawable.ic___setting_selected, getTheme()))
                .build();

        model0.setBadgeTitle("7");
        model0.showBadge();

        return new NavigationTabBarVP2.Model[] {
                model0, model1, model2
        };
    }

    @Override
    protected Fragment[] onSupplyFragments() {
        TestFragment f0 = new TestFragment();
        Bundle b0 = new Bundle();
        b0.putString(TestFragment.KEY, "要闻xxx");
        f0.setArguments(b0);

        TestFragment f1 = new TestFragment();
        Bundle b1 = new Bundle();
        b1.putString(TestFragment.KEY, "我的xxx");
        f1.setArguments(b1);

        TestFragment f2 = new TestFragment();
        Bundle b2 = new Bundle();
        b2.putString(TestFragment.KEY, "设置xxx");
        f2.setArguments(b2);

        return new Fragment[] {
                f0, f1, f2
        };
    }

    @Override
    protected int onSupplyDefaultIndex() {
        return 1;
    }

    @Override
    protected void onBackPageSelected(int position, NavigationTabBarVP2.Model model) {
        model.hideBadge();
    }
}
