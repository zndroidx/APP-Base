package com.app.base.simples.list.list;

import androidx.annotation.NonNull;

import com.app.base.R;
import com.chad.library.adapter.base.viewholder.BaseViewHolder;
import com.zndroid.base.adapter.SingleAdapter;

/**
 * @author lzy
 * @date 2021/4/1
 */
public class DanXuanAdapter extends SingleAdapter<String> {
    @Override
    protected void onBackItemSelected(@NonNull BaseViewHolder viewHolder) {
        viewHolder.getView(R.id.tv).setBackgroundColor(getContext().getResources().getColor(R.color.teal_700));
    }

    @Override
    protected void onBackItemUnSelected(@NonNull BaseViewHolder viewHolder) {
        viewHolder.getView(R.id.tv).setBackgroundColor(getContext().getResources().getColor(R.color.white));
    }

    @Override
    protected Class<String> onSupplyEntityClazz() {
        return String.class;
    }
}
