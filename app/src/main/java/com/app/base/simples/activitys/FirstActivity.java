package com.app.base.simples.activitys;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;

import androidx.activity.result.ActivityResultCallback;
import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContract;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatDelegate;

import com.app.base.R;
import com.app.base.databinding.ActivityFirstBinding;
import com.app.base.simples.fragment.DemoFragment;
import com.app.base.simples.mvp.MP;
import com.zndroid.base.ui.common.CommonActivity;
import com.zndroid.base.widgets.HolderView;
import com.zndroid.widget.title.TitleBar;

import java.util.List;

public class FirstActivity extends CommonActivity<ActivityFirstBinding, MP> {
    private TitleBar titleBar;

    @Override
    protected ActivityFirstBinding onSupplyViewBinding(@NonNull LayoutInflater layoutInflater) {
        //R.layout.activity_first
        return ActivityFirstBinding.inflate(layoutInflater);
    }

    ActivityResultLauncher launcher;

    @Override
    protected void doOnInitView(View currentView) {
        //currentView.findViewById(R.id.title_bar)
        titleBar = currentViewBinding().titleBar;
    }

    @Override
    protected void doOnCreate(@Nullable Bundle savedInstanceState) {
        super.doOnCreate(savedInstanceState);

        getSupportFragmentManager().beginTransaction().add(R.id.fragment, new DemoFragment()).commit();

        Intent intent = new Intent(Intent.ACTION_PICK);
        intent.setType("image/*");

        launcher = registerForActivityResult(new TestContract(), new ActivityResultCallback<String>() {
            @Override
            public void onActivityResult(String result) {
                showToast(result);
            }
        });
    }

    @Override
    protected int onSupplyNightModel() {
        return AppCompatDelegate.MODE_NIGHT_FOLLOW_SYSTEM;
    }

    @Override
    protected List<String> onSupplyPermissions() {
        return super.onSupplyPermissions();
    }

    @Override
    protected void onBackAllPermissionGranted() {
        super.onBackAllPermissionGranted();
        toNotKeepScreenOn();
    }

    @Override
    protected void onBackPermissionDenied(List<String> deniedList) {
        super.onBackPermissionDenied(deniedList);
        showToast("onBackPermissionDenied");
    }

    public void day(View view) {
        toChangeNightModel(false);
    }

    public void night(View view) {
        toChangeNightModel(true);
    }

    @Override
    protected TitleBar onSupplyTitleBar() {
        return titleBar;
    }

    class Params {
        int age ;
        String name;

        public int getAge() {
            return age;
        }

        public void setAge(int age) {
            this.age = age;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }
    }

    class TestContract extends ActivityResultContract<Params, String> {
        @NonNull
        @Override
        public Intent createIntent(@NonNull Context context, Params input) {
            Intent intent = new Intent(context, SecondActivity.class);
            intent.putExtra("age", input.age);
            intent.putExtra("name", input.name);
            return intent;
        }

        @Override
        public String parseResult(int resultCode, @Nullable Intent intent) {
            if (RESULT_OK == resultCode) {
                return intent.getExtras().getString("back");
            } else {
                return null;
            }
        }
    }

    public void jump(View view) {
        Params params = new Params();
        params.age = 10;
        params.name = "Tom";
        toActivityCallBackAble(launcher, params);
    }

    @Override
    public HolderView onSupplyPlaceholder() {
        //currentRootView().findViewById(R.id.hv)
        return currentViewBinding().hv;
    }
}