package com.app.base.simples.list.tree.bean;

import com.zndroid.base.model.BaseCheckableNode;

public class SecondNode extends BaseCheckableNode {
    private String title;

    public SecondNode(long id, long parentId, String title) {
        super(id, parentId);

        this.title = title;
        setExpanded(false);
    }

    public String getTitle() {
        return title;
    }

    @Override
    public String toString() {
        return "SecondNode{" +
                "childNodeSize=" + children.size() +
                ", title='" + title + '\'' +
                ", id=" + id +
                ", isChecked=" + isChecked +
                '}';
    }
}
