package com.app.base.simples.demo_edit_it;

import com.app.base.AboutActivity;
import com.app.base.MainActivity;
import com.app.base.simples.activitys.FirstActivity;
import com.app.base.simples.activitys.FragmentTestActivity;
import com.app.base.simples.activitys.MyViewPageActivity;
import com.app.base.simples.demo_edit_it.keep.MenuItem;
import com.app.base.simples.list.ListActivity;
import com.app.base.simples.ntb.MainBottomActivity;
import com.app.base.simples.splash.MySplashActivity;
import com.zndroid.base.ui.impl.BasePresenter;

import java.util.Arrays;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

/**
 * @author lzy
 * @date 2021/4/13
 *
 * 逻辑处理在这
 */
public class MainPresenter extends BasePresenter<MainActivity> {
    private MenuItem[] items;
    private final List<MenuItem> list = new CopyOnWriteArrayList<>();

    /***************************** edit it***************************/
    private void initData() {
        items = new MenuItem[] {
                new MenuItem("About", AboutActivity.class),
                new MenuItem("Splash", MySplashActivity.class),
                new MenuItem("Activity", FirstActivity.class),
                new MenuItem("Fragment", FragmentTestActivity.class),
                new MenuItem("List", ListActivity.class),
                new MenuItem("NavigationTab", MainBottomActivity.class),
                new MenuItem("View Page", MyViewPageActivity.class)
        };
    }

    @Override
    public void didOnCreate() {
        super.didOnCreate();

        getV().toEnableRefresh(false);
        getV().toEnableLoadMore(false);

        initData();
        appendList();

        getV().currentAdapter().setList(list);
    }

    private void appendList() {
        list.addAll(Arrays.asList(items));
    }

    public void jump(int index) {
        getV().jumpTo(list.get(index).getClazz());
    }
}
