package com.app.base.simples.list;

import android.view.LayoutInflater;
import android.view.View;

import androidx.annotation.NonNull;

import com.app.base.databinding.ActivityListBinding;
import com.app.base.simples.list.list.CustomSelectViewListActivity;
import com.app.base.simples.list.list.DanDuoListActivity;
import com.app.base.simples.list.list.DanXuanListActivity;
import com.app.base.simples.list.list.DefaultListActivity;
import com.app.base.simples.list.list.DuoXuanListActivity;
import com.app.base.simples.list.list.ListTempActivity;
import com.app.base.simples.list.tree.DefaultTreeListActivity;
import com.app.base.simples.list.tree.SelectableTreeListActivity;
import com.zndroid.base.simples.SimplePresenter;
import com.zndroid.base.ui.common.CommonActivity;

/**
 * @author lazy
 * @date 4/14/21
 */
public class ListActivity extends CommonActivity<ActivityListBinding, SimplePresenter> {

    @Override
    protected ActivityListBinding onSupplyViewBinding(@NonNull LayoutInflater layoutInflater) {
        //R.layout.activity_list
        return ActivityListBinding.inflate(layoutInflater);
    }

    public void defaultList(View view) {
        jumpTo(DefaultListActivity.class);
    }

    public void singleList(View view) {
        jumpTo(DanXuanListActivity.class);
    }

    public void multiList(View view) {
        jumpTo(DuoXuanListActivity.class);
    }

    public void switchList(View view) {
        jumpTo(DanDuoListActivity.class);
    }

    public void fragmentList(View view) {
        jumpTo(ListTempActivity.class);
    }

    public void treeList(View view) {
        jumpTo(DefaultTreeListActivity.class);
    }

    public void treeSelectableList(View view) {
        jumpTo(SelectableTreeListActivity.class);
    }

    public void customSelectList(View view) {
        jumpTo(CustomSelectViewListActivity.class);
    }
}
