package com.app.base.simples.list.list;

import android.os.Bundle;
import android.view.LayoutInflater;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.app.base.R;
import com.app.base.databinding.ActivityListTempBinding;
import com.zndroid.base.simples.SimplePresenter;
import com.zndroid.base.ui.common.CommonActivity;

/**
 * @author lzy
 * @date 2021/4/16
 */
public class ListTempActivity extends CommonActivity<ActivityListTempBinding, SimplePresenter> {

    @Override
    protected ActivityListTempBinding onSupplyViewBinding(@NonNull LayoutInflater layoutInflater) {
        //R.layout.activity_list_temp;
        return ActivityListTempBinding.inflate(layoutInflater);
    }

    @Override
    protected void doOnCreate(@Nullable Bundle savedInstanceState) {
        super.doOnCreate(savedInstanceState);

        getSupportFragmentManager().beginTransaction().add(R.id.fragment, new MyListFragment()).commit();
    }
}
