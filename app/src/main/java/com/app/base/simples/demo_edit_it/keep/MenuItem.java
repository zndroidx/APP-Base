package com.app.base.simples.demo_edit_it.keep;

import android.os.Build;

import java.util.Arrays;
import java.util.Objects;

/**
 * @author lzy
 * @date 2021/4/13
 */
public class MenuItem {
    private String description = "";
    private Class clazz;

    public MenuItem(String description, Class clazz) {
        this.description = description;
        this.clazz = clazz;
    }

    public String getDescription() {
        return description;
    }

    public Class getClazz() {
        return clazz;
    }

    public void setClazz(Class clazz) {
        this.clazz = clazz;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        MenuItem menuItem = (MenuItem) o;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            return Objects.equals(description, menuItem.description);
        } else {
            return description.equals(menuItem.description);
        }
    }

    @Override
    public int hashCode() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            return Objects.hash(description);
        } else {
            return Arrays.hashCode(new Object[]{description});
        }
    }
}
