package com.app.base.simples.activitys;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.app.base.databinding.ActivitySecondBinding;
import com.zndroid.base.simples.SimplePresenter;
import com.zndroid.base.ui.impl.BaseActivity;
import com.zndroid.base.widgets.SwitchButton;

/**
 * Created by lzy on 2021/1/26.
 */
public class SecondActivity extends BaseActivity<ActivitySecondBinding, SimplePresenter> {
    private TextView textView;

    @Override
    protected ActivitySecondBinding onSupplyViewBinding(@NonNull LayoutInflater layoutInflater) {
        //R.layout.activity_second
        return ActivitySecondBinding.inflate(layoutInflater);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //findViewById(R.id.show)
        textView = currentViewBinding().show;

        showToast("get data from main");
        try {
            textView.setText(getIntent().getIntExtra("age", 0) + " " + getIntent().getStringExtra("name"));
        } catch (Exception e) {
            //ignore
        }

        currentViewBinding().sb.setOnCheckedChangeListener((view, isChecked) -> toScreenCaptureEnable(isChecked));
    }

    @Override
    protected boolean isScreenCaptureAble() {
        return true;
    }

    @Override
    public void onBackPressed() {
        Intent intent = getIntent();
        intent.putExtra("back", "data has update, and you can go on");
        setResult(Activity.RESULT_OK, intent);
        super.onBackPressed();
    }
}
