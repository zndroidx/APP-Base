package com.app.base.simples.list.list;

import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.app.base.R;
import com.chad.library.adapter.base.viewholder.BaseViewHolder;
import com.zndroid.base.binder.AbsBinder;

/**
 * @author lzy
 * @date 2021/4/16
 */
public class MyItemBinder extends AbsBinder<String> {

    @Override
    public int getLayoutId() {
        return R.layout.item_list;
    }

    @Override
    public void convert(@NonNull BaseViewHolder baseViewHolder, String s) {
        TextView textView = baseViewHolder.getView(R.id.tv);
        textView.setText(s);
    }

    @Override
    public void onClick(@NonNull BaseViewHolder holder, @NonNull View view, String data, int position) {
        super.onClick(holder, view, data, position);
    }
}
