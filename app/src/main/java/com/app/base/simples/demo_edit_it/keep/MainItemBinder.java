package com.app.base.simples.demo_edit_it.keep;

import android.widget.TextView;

import com.app.base.R;
import com.chad.library.adapter.base.viewholder.BaseViewHolder;
import com.zndroid.base.binder.AbsBinder;


/**
 * @author lzy
 * @date 2021/4/13
 */
public class MainItemBinder extends AbsBinder<MenuItem> {
    @Override
    public int getLayoutId() {
        return R.layout.item_main_menu;
    }

    @Override
    public void convert(BaseViewHolder viewHolder, MenuItem menuItem) {
        ((TextView)viewHolder.getView(R.id.tv_menu)).setText(menuItem.getDescription());
    }
}
