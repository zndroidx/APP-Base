package com.app.base.simples.list.list;

import android.os.Bundle;
import android.os.CountDownTimer;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.app.base.R;
import com.app.base.databinding.ActivityDefaultListBinding;
import com.chad.library.adapter.base.viewholder.BaseViewHolder;
import com.zndroid.base.binder.AbsBinder;
import com.zndroid.base.simples.SimplePresenter;
import com.zndroid.base.ui.common.CommonListActivity;
import com.zndroid.base.widgets.HolderView;


import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

/**
 * Created by lzy on 2021/3/26.
 */
public class DefaultListActivity extends CommonListActivity<ActivityDefaultListBinding, SimplePresenter, DefAdapter, String> {
    @Override
    protected DefAdapter onSupplyAdapter() {
        return new DefAdapter();
    }

    @Override
    protected AbsBinder<String> onSupplyItemBinder() {
        return new MyItem();
    }

    @Override
    public HolderView onSupplyPlaceholder() {
        //findViewById(R.id.hv);
        return currentViewBinding().hv;
    }

    @Override
    protected RecyclerView onSupplyRecyclerView() {
        //findViewById(R.id.rv);
        return currentViewBinding().rv;
    }

    @Override
    protected ActivityDefaultListBinding onSupplyViewBinding(@NonNull LayoutInflater layoutInflater) {
        //R.layout.activity_default_list;
        return ActivityDefaultListBinding.inflate(layoutInflater);
    }

    @Override
    protected SwipeRefreshLayout onSupplyRefreshLayout() {
        //findViewById(R.id.sw);
        return currentViewBinding().sw;
    }

    @Override
    public void onBackRetryClicked(View v) {
        new CountDownTimer(3000, 1000) {

            @Override
            public void onTick(long millisUntilFinished) {

            }

            @Override
            public void onFinish() {
                initData();
                toDismissLoadMore(true);
                toDismissPlaceholder();
            }
        }.start();
    }

    @Override
    protected void onBackLoadMore() {
        new CountDownTimer(3000, 1000) {

            @Override
            public void onTick(long millisUntilFinished) {

            }

            @Override
            public void onFinish() {
                toDismissLoadMore(true);
                showErrorData();
            }
        }.start();
    }

    @Override
    protected void onBackRefreshing() {
        super.onBackRefreshing();

        new CountDownTimer(3000, 1000) {

            @Override
            public void onTick(long millisUntilFinished) {

            }

            @Override
            public void onFinish() {
                initData();
                toDismissRefresh();
            }
        }.start();

    }

    private void initData2() {
        List<String> list = new CopyOnWriteArrayList<>();
        list.add("haha");
        list.add("xx");

        currentAdapter().setList(list);
    }

    private void initData() {
        List<String> list = new CopyOnWriteArrayList<>();
        list.add("haha");
        list.add("xx");
        list.add("ggg");
        list.add("fufuf");
        list.add("haha");
        list.add("xx");
        list.add("ggg");
        list.add("fufuf");
        list.add("haha");
        list.add("xx");
        list.add("ggg");
        list.add("fufuf");
        list.add("haha");
        list.add("xx");
        list.add("ggg");
        list.add("fufuf");
        list.add("haha");
        list.add("xx");
        list.add("ggg");
        list.add("fufuf");

        currentAdapter().setList(list);
    }

    @Override
    protected void doOnCreate(@Nullable Bundle savedInstanceState) {
        super.doOnCreate(savedInstanceState);

        initData();
    }

    class MyItem extends AbsBinder<String> {

        @Override
        public int getLayoutId() {
            return R.layout.item_list;
        }

        @Override
        public void convert(@NonNull BaseViewHolder baseViewHolder, String s) {
            TextView textView = baseViewHolder.getView(R.id.tv);
            textView.setText(s);
        }

        @Override
        public void onClick(@NonNull BaseViewHolder holder, @NonNull View view, String data, int position) {
            super.onClick(holder, view, data, position);
            showToast(data);
        }
    }
}
