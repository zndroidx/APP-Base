package com.app.base.simples.list.tree.provider;

import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;

import androidx.annotation.NonNull;

import com.app.base.R;
import com.app.base.simples.list.tree.DefaultTreeAdapter;
import com.app.base.simples.list.tree.bean.ThirdNode;
import com.chad.library.adapter.base.entity.node.BaseNode;
import com.chad.library.adapter.base.viewholder.BaseViewHolder;
import com.zndroid.base.binder.AbsProvider;

public class ThirdProvider extends AbsProvider<DefaultTreeAdapter> {

    @Override
    public int getItemViewType() {
        return 3;
    }

    @Override
    public int getLayoutId() {
        return R.layout.item_node_third;
    }

    @Override
    public void convert(@NonNull BaseViewHolder helper, @NonNull BaseNode data) {
        ThirdNode entity = (ThirdNode) data;
        helper.setText(R.id.title, entity.getTitle() + " id= " + entity.getId());

        if (getAdapter().isSelectEnable()) {
            helper.getView(R.id.cb).setVisibility(View.VISIBLE);
        } else {
            helper.getView(R.id.cb).setVisibility(View.GONE);
        }

        CheckBox checkBox = helper.getView(R.id.cb);
        checkBox.setChecked(entity.isChecked());
        checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (buttonView.isPressed()) {
                    getAdapter().toSelectItem(data, isChecked);
                }
            }
        });
    }

    @Override
    public void onClick(@NonNull BaseViewHolder helper, @NonNull View view, BaseNode data, int position) {

    }
}
