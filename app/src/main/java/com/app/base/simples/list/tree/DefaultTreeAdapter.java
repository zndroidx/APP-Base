package com.app.base.simples.list.tree;

import com.app.base.simples.list.tree.bean.FirstNode;
import com.app.base.simples.list.tree.bean.SecondNode;
import com.app.base.simples.list.tree.bean.ThirdNode;
import com.chad.library.adapter.base.entity.node.BaseNode;
import com.zndroid.base.adapter.BaseNodeAdapter;

import java.util.List;

/**
 * @author lzy
 * @date 2021/5/19
 */
public class DefaultTreeAdapter extends BaseNodeAdapter {
    @Override
    protected int getItemType(List<? extends BaseNode> list, int position) {
        BaseNode node = list.get(position);
        if (node instanceof FirstNode) {
            return 1;
        } else if (node instanceof SecondNode) {
            return 2;
        } else if (node instanceof ThirdNode) {
            return 3;
        }
        return -1;
    }
}
