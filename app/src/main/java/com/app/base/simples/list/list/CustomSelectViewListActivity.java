package com.app.base.simples.list.list;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.app.base.R;
import com.app.base.databinding.ActivityCustomSelectViewListBinding;
import com.chad.library.adapter.base.viewholder.BaseViewHolder;
import com.zndroid.base.annotation.IAdapterType;
import com.zndroid.base.binder.AbsBinder;
import com.zndroid.base.simples.SimplePresenter;
import com.zndroid.base.ui.common.CommonListActivity;

import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

public class CustomSelectViewListActivity extends CommonListActivity<ActivityCustomSelectViewListBinding, SimplePresenter, CustomSelectViewAdapter, String> {
    @Override
    protected CustomSelectViewAdapter onSupplyAdapter() {
        // 单一原则：和数据相关的都在adapter中处理
        return new CustomSelectViewAdapter();
    }

    @Override
    protected CustomSelectViewBinder onSupplyItemBinder() {
        CustomSelectViewBinder binder = new CustomSelectViewBinder();
        // 单一原则：和绑定相关的都在binder中处理
        binder.addChildClickViewIds(R.id.btn);

        /*指定选中控件，需要将其加入可点击列表*/
        binder.addChildClickViewIds(R.id.cb);
        binder.addChildClickViewIds(R.id.iv);

        //方式一：调用方法(根据开发经验，选中功能基本上都是一个控件进行控制)
        //way1: 使用Checkable控件实现的
        binder.setSupportSelectViewId(R.id.cb);
        //way2: 使用非Checkable控件实现的
//        binder.setSupportSelectViewId(R.id.iv);
        return binder;
    }

    @Override
    protected RecyclerView onSupplyRecyclerView() {
        return currentViewBinding().rv;
    }

    @Override
    protected SwipeRefreshLayout onSupplyRefreshLayout() {
        return currentViewBinding().sw;
    }

    @Override
    protected ActivityCustomSelectViewListBinding onSupplyViewBinding(@NonNull LayoutInflater layoutInflater) {
        return ActivityCustomSelectViewListBinding.inflate(layoutInflater);
    }

    @Override
    protected void onBackItemClicked(@NonNull View view, int position, @Nullable String s) {
        showToast("当前点击item：" + s + " 位置= " + position);
    }

    @Override
    protected void onBackItemChildClicked(@NonNull View view, int position, @Nullable String s) {
        showToast("当前点击子控件View：" + view.getClass().getSimpleName() + " item= " + s + " 位置= " + position);
    }

    @Override
    protected void onBackDataChanged(List<String> list) {
        logE("选中 = " + list.size() + " 条");
    }

    @Override
    protected void doOnCreate(@Nullable Bundle savedInstanceState) {
        super.doOnCreate(savedInstanceState);
        initData();

        toEnableLoadMore(false);
        toEnableRefresh(false);
    }

    private void initData() {
        List<String> list = new CopyOnWriteArrayList<>();
        list.add("haha");
        list.add("xx");
        list.add("ggg");
        list.add("hj");
        list.add("q4234");
        list.add("gfj");
        list.add("qwr423t");
        list.add("123");
        list.add("045860");
        list.add("678-6078=-67");
        list.add("6456");
        list.add("000009");
        list.add("0000099d");
        list.add("123ndskfn");
        list.add("124dgf");
        list.add("ertgv");
        list.add("iiepritpwe");
        list.add("asjoqwur");
        list.add("wur");
        list.add("oio");


        currentAdapter().setList(list);

        List<String> se = new CopyOnWriteArrayList<>();
        se.add("haha");

        currentAdapter().toSelect(se);
    }

    class CustomSelectViewBinder extends AbsBinder<String> {

        @Override
        public void convert(@NonNull BaseViewHolder baseViewHolder, String s) {
            TextView textView = baseViewHolder.getView(R.id.tv);
            textView.setText(s);
        }

        @Override
        public int getLayoutId() {
            return R.layout.item_list2;
        }

        //方式二：复写
//        @Override
//        public int onSupplySelectId() {
//            return R.id.cb;
//        }
    }

    public void switchDefault(View view) {
        currentAdapter().toSwitchType(IAdapterType.DEF);
    }

    public void switchSingle(View view) {
        currentAdapter().toSwitchType(IAdapterType.SINGLE);
    }

    public void switchMulti(View view) {
        currentAdapter().toSwitchType(IAdapterType.MULTI);
    }
}
