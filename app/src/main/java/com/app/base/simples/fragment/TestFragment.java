package com.app.base.simples.fragment;

import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.app.base.databinding.LayoutTestFragmentBinding;
import com.zndroid.base.simples.SimplePresenter;
import com.zndroid.base.ui.common.CommonFragment;

public class TestFragment extends CommonFragment<LayoutTestFragmentBinding, SimplePresenter> {
    public static final String KEY = "v";
    public static final String KEY_COLOR = "c";

    @Override
    protected boolean isValidateStatueBarColorOfFragment() {
        return true;
    }

    @Override
    protected LayoutTestFragmentBinding onSupplyViewBinding(@NonNull LayoutInflater layoutInflater, @Nullable ViewGroup parent) {
        //R.layout.layout_test_fragment;
        return LayoutTestFragmentBinding.inflate(layoutInflater, parent, false);
    }

    @Override
    public void onResume() {
        super.onResume();

        int color = getBundleArgs().getInt(KEY_COLOR);
        toChangeStatusBarColor(color, 0);

        //currentRootView().findViewById(R.id.tv)
        TextView textView = currentViewBinding().tv;
        textView.setText(getBundleArgs().getString(KEY));
    }
}
