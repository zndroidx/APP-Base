package com.app.base.simples.fragment;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.res.ResourcesCompat;

import com.app.base.R;
import com.app.base.databinding.LayoutTestFragmentBinding;
import com.zndroid.base.simples.SimplePresenter;
import com.zndroid.base.ui.common.CommonFragment;

public class DemoFragment extends CommonFragment<LayoutTestFragmentBinding, SimplePresenter> {
    @Override
    protected LayoutTestFragmentBinding onSupplyViewBinding(@NonNull LayoutInflater layoutInflater, @Nullable ViewGroup parent) {
        return LayoutTestFragmentBinding.inflate(layoutInflater, parent, false);
    }

    @Override
    protected int onSupplyStatusBarColor() {
        return ResourcesCompat.getColor(getResources(), R.color.design_default_color_primary_dark, requireActivity().getTheme());
    }

    @Override
    protected boolean isValidateStatueBarColorOfFragment() {
        return true;
    }
}
