package com.app.base.simples.list.list;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.app.base.R;
import com.app.base.databinding.ActivityDanDuoListBinding;
import com.chad.library.adapter.base.viewholder.BaseViewHolder;
import com.zndroid.base.annotation.IAdapterType;
import com.zndroid.base.binder.AbsBinder;
import com.zndroid.base.simples.SimplePresenter;
import com.zndroid.base.ui.common.CommonListActivity;
import com.zndroid.base.widgets.HolderView;


import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

/**
 * @author lzy
 * @date 2021/4/1
 */
public class DanDuoListActivity extends CommonListActivity<ActivityDanDuoListBinding, SimplePresenter, DuoXuanAdapter, String> {

    @Override
    protected DuoXuanAdapter onSupplyAdapter() {
        DuoXuanAdapter adapter = new DuoXuanAdapter();
        adapter.toSwitchType(IAdapterType.SINGLE);
        return adapter;
    }

    @Override
    protected AbsBinder<String> onSupplyItemBinder() {
        return new MyItem();
    }

    @Override
    public HolderView onSupplyPlaceholder() {
        //findViewById(R.id.hv);
        return currentViewBinding().hv;
    }

    @Override
    protected RecyclerView onSupplyRecyclerView() {
        //findViewById(R.id.rv);
        return currentViewBinding().rv;
    }

    @Override
    protected ActivityDanDuoListBinding onSupplyViewBinding(@NonNull LayoutInflater layoutInflater) {
        //R.layout.activity_dan_duo_list;
        return ActivityDanDuoListBinding.inflate(layoutInflater);
    }

    @Override
    protected SwipeRefreshLayout onSupplyRefreshLayout() {
        //findViewById(R.id.sw);
        return currentViewBinding().sw;
    }

    @Override
    protected void doOnCreate(@Nullable Bundle savedInstanceState) {
        super.doOnCreate(savedInstanceState);
        initData();

        toEnableLoadMore(false);
        toEnableRefresh(false);
    }

    private void initData() {
        List<String> list = new CopyOnWriteArrayList<>();
        list.add("haha");
        list.add("xx");
        list.add("ggg");
        list.add("hj");
        list.add("q4234");
        list.add("gfj");
        list.add("qwr423t");
        list.add("123");
        list.add("045860");
        list.add("678-6078=-67");
        list.add("6456");
        list.add("000009");
        list.add("0000099d");
        list.add("123ndskfn");
        list.add("124dgf");
        list.add("ertgv");
        list.add("iiepritpwe");
        list.add("asjoqwur");
        list.add("wur");
        list.add("oio");


        currentAdapter().setList(list);

        List<String> se = new CopyOnWriteArrayList<>();
        se.add("haha");

        currentAdapter().toSelect(se);
    }

    public void switchDefault(View view) {
        currentAdapter().toSwitchType(IAdapterType.DEF);
    }

    public void switchSingle(View view) {
        currentAdapter().toSwitchType(IAdapterType.SINGLE);
    }

    public void switchMulti(View view) {
        currentAdapter().toSwitchType(IAdapterType.MULTI);
    }

    class MyItem extends AbsBinder<String> {

        @Override
        public int getLayoutId() {
            return R.layout.item_list;
        }

        @Override
        public void convert(@NonNull BaseViewHolder baseViewHolder, String s) {
            TextView textView = baseViewHolder.getView(R.id.tv);
            textView.setText(s);
        }

        @Override
        public void onClick(@NonNull BaseViewHolder holder, @NonNull View view, String data, int position) {
            super.onClick(holder, view, data, position);
            showToast(data);
        }

        @Override
        public boolean onLongClick(@NonNull BaseViewHolder holder, @NonNull View view, String data, int position) {
            return false;
        }

        @Override
        public void onChildClick(@NonNull BaseViewHolder holder, @NonNull View view, String data, int position) {
            super.onChildClick(holder, view, data, position);
        }

        @Override
        public boolean onChildLongClick(@NonNull BaseViewHolder holder, @NonNull View view, String data, int position) {
            return false;
        }
    }

    @Override
    protected void onBackItemClicked(@NonNull View view, int position, @Nullable String s) {
        showToast("onBackItemClicked = " + s);
    }

    @Override
    protected boolean onBackItemLongClicked(@NonNull View view, int position, @Nullable String s) {
        return false;
    }

    @Override
    protected void onBackItemChildClicked(@NonNull View view, int position, @Nullable String s) {
        showToast("onBackItemChildClicked = " + s);

    }

    @Override
    protected boolean onBackItemChildLongClicked(@NonNull View view, int position, @Nullable String s) {
        return false;
    }

    @Override
    protected void onBackDataChanged(List<String> list) {
        super.onBackDataChanged(list);
        showToast("已选中 " + list.size() + " 条");
    }
}
