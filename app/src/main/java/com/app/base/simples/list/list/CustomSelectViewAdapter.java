package com.app.base.simples.list.list;

import android.widget.CheckBox;
import android.widget.ImageView;

import androidx.annotation.NonNull;

import com.app.base.R;
import com.chad.library.adapter.base.viewholder.BaseViewHolder;
import com.zndroid.base.adapter.MultiAdapter;

public class CustomSelectViewAdapter extends MultiAdapter<String> {
    @Override
    protected void onBackItemSelected(@NonNull BaseViewHolder viewHolder) {
        //way1: 使用Checkable控件实现的
        CheckBox checkBox = viewHolder.getView(R.id.cb);
        checkBox.setChecked(true);

        //way2: 使用非Checkable控件实现的
        ImageView imageView = viewHolder.getView(R.id.iv);
        imageView.setImageResource(R.drawable.z_base_ic_selected);
    }

    @Override
    protected void onBackItemUnSelected(@NonNull BaseViewHolder viewHolder) {
        //way1: 使用Checkable控件实现的
        CheckBox checkBox = viewHolder.getView(R.id.cb);
        checkBox.setChecked(false);

        //way2: 使用非Checkable控件实现的
        ImageView imageView = viewHolder.getView(R.id.iv);
        imageView.setImageResource(R.drawable.z_base_ic_unselect);
    }

    @Override
    protected Class<String> onSupplyEntityClazz() {
        return String.class;
    }
}
