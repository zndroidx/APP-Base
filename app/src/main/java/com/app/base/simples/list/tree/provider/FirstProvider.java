package com.app.base.simples.list.tree.provider;

import android.view.View;
import android.view.animation.DecelerateInterpolator;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.core.view.ViewCompat;

import com.app.base.R;
import com.app.base.simples.list.tree.DefaultTreeAdapter;
import com.app.base.simples.list.tree.bean.FirstNode;
import com.chad.library.adapter.base.entity.node.BaseNode;
import com.chad.library.adapter.base.viewholder.BaseViewHolder;
import com.zndroid.base.binder.AbsProvider;


import java.util.List;
import java.util.Objects;

public class FirstProvider extends AbsProvider<DefaultTreeAdapter> {

    @Override
    public int getItemViewType() {
        return 1;
    }

    @Override
    public int getLayoutId() {
        return R.layout.item_node_first;
    }

    @Override
    public void convert(@NonNull BaseViewHolder helper, @NonNull BaseNode data) {
        FirstNode entity = (FirstNode) data;
        helper.setText(R.id.title, entity.getTitle());
        helper.setImageResource(R.id.iv, R.drawable.icon_arrow);
        helper.setText(R.id.num, Objects.requireNonNull("id= " + ((FirstNode) data).getId()));

        if (getAdapter().isSelectEnable()) {
            helper.getView(R.id.cb).setVisibility(View.VISIBLE);
        } else {
            helper.getView(R.id.cb).setVisibility(View.GONE);
        }

        setArrowSpin(helper, data, true);

        CheckBox checkBox = helper.getView(R.id.cb);
        checkBox.setChecked(entity.isChecked());
        checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (buttonView.isPressed()) {
                    getAdapter().toSelectItem(data, isChecked);
                }
            }
        });
    }

    @Override
    public void convert(@NonNull BaseViewHolder helper, @NonNull BaseNode data, @NonNull List<?> payloads) {
        for (Object payload : payloads) {
            if (payload instanceof Integer && (int) payload == 110) {
                // 增量刷新，使用动画变化箭头
                setArrowSpin(helper, data, true);
            }
        }
    }

    private void setArrowSpin(BaseViewHolder helper, BaseNode data, boolean isAnimate) {
        ImageView imageView = helper.getView(R.id.iv);

        if (!getAdapter().isNotEmpty(data.getChildNode())) {
            imageView.setVisibility(View.INVISIBLE);
            return;
        }

        imageView.setVisibility(View.VISIBLE);

        FirstNode entity = (FirstNode) data;


        if (entity.isExpanded()) {
            if (isAnimate) {
                ViewCompat.animate(imageView).setDuration(200)
                        .setInterpolator(new DecelerateInterpolator())
                        .rotation(90f)
                        .start();
            } else {
                imageView.setRotation(90f);
            }
        } else {
            if (isAnimate) {
                ViewCompat.animate(imageView).setDuration(200)
                        .setInterpolator(new DecelerateInterpolator())
                        .rotation(0f)
                        .start();
            } else {
                imageView.setRotation(0f);
            }
        }
    }

    @Override
    public void onClick(@NonNull BaseViewHolder helper, @NonNull View view, BaseNode data, int position) {
        // 这里使用payload进行增量刷新（避免整个item刷新导致的闪烁，不自然）
        getAdapter().expandOrCollapse(position, true, true);
    }
}
