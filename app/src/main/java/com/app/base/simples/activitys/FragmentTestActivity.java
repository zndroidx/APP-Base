package com.app.base.simples.activitys;

import android.os.Bundle;
import android.view.LayoutInflater;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.app.base.R;
import com.app.base.databinding.ActivityFragmentTestBinding;
import com.app.base.simples.fragment.MyFragment;
import com.zndroid.base.simples.SimplePresenter;
import com.zndroid.base.ui.impl.BaseActivity;

/**
 * Created by lzy on 2021/1/26.
 */
public class FragmentTestActivity extends BaseActivity<ActivityFragmentTestBinding, SimplePresenter> {

    @Override
    protected ActivityFragmentTestBinding onSupplyViewBinding(@NonNull LayoutInflater layoutInflater) {
        return ActivityFragmentTestBinding.inflate(layoutInflater);
    }

    @Override
    protected void doOnCreate(@Nullable Bundle savedInstanceState) {
        super.doOnCreate(savedInstanceState);

        getSupportFragmentManager().beginTransaction().add(R.id.fragment, new MyFragment()).commit();
    }
}
