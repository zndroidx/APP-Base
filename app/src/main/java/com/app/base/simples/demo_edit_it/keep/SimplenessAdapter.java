package com.app.base.simples.demo_edit_it.keep;

import com.zndroid.base.adapter.DefaultAdapter;

/**
 * @author lzy
 * @date 2021/4/13
 */
public class SimplenessAdapter extends DefaultAdapter<MenuItem> {
    @Override
    protected Class<MenuItem> onSupplyEntityClazz() {
        return MenuItem.class;
    }
}
