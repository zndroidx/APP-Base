package com.app.base.simples.list.tree.bean;

import androidx.annotation.Nullable;

import com.chad.library.adapter.base.entity.node.BaseNode;
import com.zndroid.base.model.BaseCheckableNode;

import java.util.List;

public class ThirdNode extends BaseCheckableNode {
    private String title;

    public ThirdNode(long id, long parentId, String title) {
        super(id, parentId);

        this.title = title;
    }

    public String getTitle() {
        return title;
    }

    @Nullable
    @Override
    public List<BaseNode> getChildNode() {
        return null;
    }

    @Override
    public String toString() {
        return "ThirdNode{" +
                "title='" + title + '\'' +
                ", id=" + id +
                ", isChecked=" + isChecked +
                '}';
    }
}
