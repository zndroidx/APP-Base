package com.app.base.simples.list.list;

import androidx.annotation.NonNull;

import com.chad.library.adapter.base.viewholder.BaseViewHolder;
import com.zndroid.base.adapter.BaseAdapter;

/**
 * Created by lzy on 2021/3/26.
 */
public class DefAdapter extends BaseAdapter<String> {
    @Override
    protected void onBackItemSelected(@NonNull BaseViewHolder viewHolder) {

    }

    @Override
    protected void onBackItemUnSelected(@NonNull BaseViewHolder viewHolder) {

    }

    @Override
    protected Class<String> onSupplyEntityClazz() {
        return String.class;
    }
}
