package com.app.base.simples.list.list;

import android.os.Bundle;

import androidx.annotation.Nullable;

import com.zndroid.base.binder.AbsBinder;
import com.zndroid.base.simples.SimpleListFragment;
import com.zndroid.base.simples.SimplePresenter;

import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

/**
 * @author lzy
 * @date 2021/4/16
 */
public class MyListFragment extends SimpleListFragment<SimplePresenter, DuoXuanAdapter, String> {
    @Override
    protected DuoXuanAdapter onSupplyAdapter() {
        return new DuoXuanAdapter();
    }

    @Override
    protected AbsBinder<String> onSupplyItemBinder() {
        return new MyItemBinder();
    }

    @Override
    protected void doOnCreate(@Nullable Bundle savedInstanceState) {
        super.doOnCreate(savedInstanceState);

        initData();

        toEnableLoadMore(true);
        toEnableRefresh(true);
    }

    private void initData() {
        List<String> list = new CopyOnWriteArrayList<>();
        list.add("haha");
        list.add("xx");
        list.add("ggg");
        list.add("hj");
        list.add("q4234");
        list.add("gfj");
        list.add("qwr423t");
        list.add("123");
        list.add("045860");
        list.add("678-6078=-67");
        list.add("6456");
        list.add("000009");
        list.add("0000099d");
        list.add("123ndskfn");
        list.add("124dgf");
        list.add("ertgv");
        list.add("iiepritpwe");
        list.add("asjoqwur");
        list.add("wur");
        list.add("oio");


        currentAdapter().setList(list);

        List<String> se = new CopyOnWriteArrayList<>();
        se.add("123");
        se.add("haha");
        se.add("oio");

        currentAdapter().toSelect(se);
    }

    @Override
    protected void onBackDataChanged(List<String> list) {
        super.onBackDataChanged(list);
        showToast(list.size() + " has been selected");
    }

    @Override
    protected boolean isSupportSearchMenu() {
        return true;
    }

    @Override
    protected boolean isSupportSelectMenu() {
        return true;
    }

    @Override
    protected boolean isSupportShowLineMenu() {
        return true;
    }

    @Override
    protected void onBackToSearch(boolean isSearch, String input) {
        showToast(isSearch + " " + input);
    }

    @Override
    protected void onBackSearchTextChanged(String text) {
        showToast(text);
    }
}
