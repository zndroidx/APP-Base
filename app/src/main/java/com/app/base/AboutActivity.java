package com.app.base;

import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.webkit.WebResourceRequest;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.app.base.databinding.ActivityAboutBinding;
import com.zndroid.base.ui.common.CommonActivity;
import com.zndroid.base.ui.impl.BasePresenter;

/**
 * @author lazy
 * @date 4/14/21
 */
public class AboutActivity extends CommonActivity<ActivityAboutBinding, AboutActivity.AboutPresenter> {
    private WebView webView;

    @Override
    protected ActivityAboutBinding onSupplyViewBinding(@NonNull LayoutInflater layoutInflater) {
        //R.layout.activity_about
        return ActivityAboutBinding.inflate(layoutInflater);
    }

    @Override
    protected void doOnCreate(@Nullable Bundle savedInstanceState) {
        super.doOnCreate(savedInstanceState);

        logI("about act doOnCreate");

        String url = "https://gitee.com/zndroidx/APP-Base";
        //findViewById(R.id.sample_wv);
        webView = currentViewBinding().sampleWv;
        webView.getSettings().setJavaScriptEnabled(true);
        webView.loadUrl(url);
        webView.setWebViewClient(new WebViewClient(){
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, WebResourceRequest request) {
                view.loadUrl(url);
                return true;
            }
        });
    }

    @Override
    protected void doOnResume() {
        super.doOnResume();

        logI("about act doOnResume");
        webView.onResume();
    }

    @Override
    protected void doOnPause() {
        super.doOnPause();

        logI("about act doOnPause");
        webView.onPause();
    }

    @Override
    protected void doOnDestroy() {
        webView.destroy();
        super.doOnDestroy();

        logI("about act doOnDestroy");
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        logI("about act onCreate");
    }

    @Override
    protected void onResume() {
        super.onResume();
        logI("about act onResume");
    }

    @Override
    protected void onPostResume() {
        super.onPostResume();
        logI("about act onPostResume");
    }

    @Override
    protected void onPause() {
        super.onPause();
        logI("about act onPause");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        logI("about act onDestroy");
    }

    @Override
    protected void onPostCreate(@Nullable Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        logI("about act onPostCreate");
    }

    @Override
    protected void doOnConfigurationChanged(@NonNull Configuration newConfig) {
        super.doOnConfigurationChanged(newConfig);
        logI("about act doOnConfigurationChanged");
    }

    @Override
    public void onConfigurationChanged(@NonNull Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        logI("about act onConfigurationChanged");
    }

    @Override
    protected void doOnStart() {
        super.doOnStart();
        logI("about act doOnStart");
    }

    @Override
    protected void onStart() {
        super.onStart();
        logI("about act onStart");
    }

    @Override
    protected void doOnStop() {
        super.doOnStop();
        logI("about act doOnStop");
    }

    @Override
    protected void onStop() {
        super.onStop();
        logI("about act onStop");
    }

    @Override
    protected void doSaveInstanceState(@NonNull Bundle outState) {
        super.doSaveInstanceState(outState);
        logI("about act doSaveInstanceState");
    }

    @Override
    protected void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        logI("about act onSaveInstanceState");
    }

    @Override
    protected void doOnNewIntent(Intent intent) {
        super.doOnNewIntent(intent);
        logI("about act doOnNewIntent");
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        logI("about act onNewIntent");
    }

    @Override
    protected void doPreOnBackPressed() {
        super.doPreOnBackPressed();
        logI("about act doPreOnBackPressed");
    }

    @Override
    protected void doOnBackPressed() {
        super.doOnBackPressed();
        logI("about act doOnBackPressed");
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        logI("about act onBackPressed");
    }

    @Override
    protected void doOnRestart() {
        super.doOnRestart();
        logI("about act doOnRestart");
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        logI("about act onRestart");
    }

    @Override
    protected AboutPresenter onSupplyPresenter() {
        return new AboutPresenter();
    }

    class AboutPresenter extends BasePresenter<AboutActivity> {
        @Override
        public void didOnCreate() {
            super.didOnCreate();
            logI("about ppp didOnCreate");
        }

        @Override
        protected void didOnStart() {
            super.didOnStart();
            logI("about ppp didOnStart");
        }

        @Override
        public void didOnResume() {
            super.didOnResume();
            logI("about ppp didOnResume");
        }

        @Override
        protected void didOnPause() {
            super.didOnPause();
            logI("about ppp didOnPause");
        }

        @Override
        protected void didOnStop() {
            super.didOnStop();
            logI("about ppp didOnStop");
        }

        @Override
        protected void didOnDestroy() {
            super.didOnDestroy();
            logI("about ppp didOnDestroy");
        }

        @Override
        protected void didOnBackPressed() {
            super.didOnBackPressed();
            logI("about ppp didOnBackPressed");
        }
    }
}
