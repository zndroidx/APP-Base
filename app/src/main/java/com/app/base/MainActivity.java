package com.app.base;

import android.view.LayoutInflater;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.app.base.databinding.ActivityMainBinding;
import com.app.base.simples.demo_edit_it.keep.MainItemBinder;
import com.app.base.simples.demo_edit_it.MainPresenter;
import com.app.base.simples.demo_edit_it.keep.MenuItem;
import com.app.base.simples.demo_edit_it.keep.SimplenessAdapter;
import com.chad.library.adapter.base.binder.BaseItemBinder;
import com.zndroid.base.ui.common.CommonListActivity;
import com.zndroid.base.widgets.HolderView;

import java.util.List;

/**
 * @author lzy
 * @date 2021/4/13
 */
public class MainActivity extends CommonListActivity<ActivityMainBinding, MainPresenter, SimplenessAdapter, MenuItem> {

    @Override
    protected SimplenessAdapter onSupplyAdapter() {
        return new SimplenessAdapter();
    }

    @Override
    protected BaseItemBinder onSupplyItemBinder() {
        return new MainItemBinder();
    }

    @Override
    protected RecyclerView onSupplyRecyclerView() {
        //findViewById(R.id.simple_rv);
        return currentViewBinding().simpleRv;
    }

    @Override
    protected SwipeRefreshLayout onSupplyRefreshLayout() {
        //findViewById(R.id.simple_sw);
        return currentViewBinding().simpleSw;
    }

    @Override
    protected ActivityMainBinding onSupplyViewBinding(@NonNull LayoutInflater layoutInflater) {
        //R.layout.activity_main;
        return ActivityMainBinding.inflate(layoutInflater);
    }

    @Override
    public HolderView onSupplyPlaceholder() {
        //findViewById(R.id.simple_hv);
        return currentViewBinding().simpleHv;
    }

    @Override
    protected MainPresenter onSupplyPresenter() {
        return new MainPresenter();
    }

    @Override
    protected RecyclerView.LayoutManager onSupplyLayoutManager() {
        return new GridLayoutManager(this, 3);
    }

    @Override
    protected RecyclerView.ItemDecoration onSupplyItemDecoration() {
        return null;
    }

    @Override
    protected void onBackItemClicked(@NonNull View view, int position, @Nullable MenuItem item) {
        super.onBackItemClicked(view, position, item);
        currentPresenter().jump(position);
    }

    @Override
    protected boolean isFullScreen() {
        return false;
    }

    @Override
    protected void onBackAllPermissionGranted() {
        super.onBackAllPermissionGranted();
        showToast("all permission is ok");
    }

    @Override
    protected void onBackPermissionDenied(List<String> deniedList) {
        super.onBackPermissionDenied(deniedList);
        showToast("has permission not ok");
    }
}
