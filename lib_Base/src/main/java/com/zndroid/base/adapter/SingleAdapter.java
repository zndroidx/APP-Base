package com.zndroid.base.adapter;

import androidx.annotation.Nullable;

import com.zndroid.base.annotation.IAdapterType;

import java.util.List;

/**
 * @author lazy
 * @date 3/25/21
 */
@SuppressWarnings("unused")
public abstract class SingleAdapter<T> extends BaseAdapter<T> {
    public SingleAdapter() {
        super(IAdapterType.SINGLE);
    }

    public SingleAdapter(@Nullable List<Object> list) {
        super(IAdapterType.SINGLE, list);
    }
}
