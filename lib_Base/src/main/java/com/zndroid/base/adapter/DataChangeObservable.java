package com.zndroid.base.adapter;

import java.util.List;
import java.util.Observable;

/**
 * @author lzy
 * @date 2021/4/2
 */
public class DataChangeObservable<T> extends Observable {

    public void dataChanged(List<T> list) {
        setChanged();
        notifyObservers(list);
    }
}
