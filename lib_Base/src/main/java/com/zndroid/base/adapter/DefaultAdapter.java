package com.zndroid.base.adapter;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.chad.library.adapter.base.viewholder.BaseViewHolder;
import com.zndroid.base.annotation.IAdapterType;

import java.util.List;

/**
 * @author lazy
 * @date 3/25/21
 */
public abstract class DefaultAdapter<T> extends BaseAdapter<T> {

    public DefaultAdapter() {
        super(IAdapterType.DEF);
    }

    public DefaultAdapter(@Nullable List<Object> list) {
        super(IAdapterType.DEF, list);
    }

    @Override
    protected void onBackItemSelected(@NonNull BaseViewHolder viewHolder) {

    }

    @Override
    protected void onBackItemUnSelected(@NonNull BaseViewHolder viewHolder) {

    }
}
