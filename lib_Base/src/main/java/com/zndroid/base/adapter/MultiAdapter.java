package com.zndroid.base.adapter;

import androidx.annotation.Nullable;

import com.zndroid.base.annotation.IAdapterType;

import java.util.List;

/**
 * @author lazy
 * @date 3/25/21
 */
@SuppressWarnings("unused")
public abstract class MultiAdapter<T> extends BaseAdapter<T> {
    public MultiAdapter() {
        super(IAdapterType.MULTI);
    }

    public MultiAdapter(@Nullable List<Object> list) {
        super(IAdapterType.MULTI, list);
    }
}
