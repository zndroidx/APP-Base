package com.zndroid.base.callback;

import android.view.MotionEvent;
import android.view.View;

import androidx.annotation.NonNull;

/**
 * @author lzy
 */
@SuppressWarnings("unused")
public interface IOnTouchListener {
    /**
     * common view touch
     * @param view view
     * @param event event
     * @return true or false
     * */
    boolean onBackTouched(@NonNull View view, @NonNull MotionEvent event);
}
