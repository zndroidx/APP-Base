package com.zndroid.base.callback;

import android.view.View;

import androidx.annotation.NonNull;

/**
 * @author lzy
 */
@SuppressWarnings("unused")
public interface IOnClickListener {
    /**
     * common view onclick
     * @param view view
     * */
    void onBackClicked(@NonNull View view);
}
