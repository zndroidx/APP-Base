package com.zndroid.base.callback;

import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

/**
 * @author lzy
 */
public interface IOnItemChildLongClickListener<T> {
    /**
     * common item long click
     * @param view view
     * @param position position
     * @param t Object if exist
     *
     * @return true or false
     * */
    boolean onBackItemChildLongClicked(@NonNull View view, int position, @Nullable T t);
}
