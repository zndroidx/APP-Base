package com.zndroid.base.callback;

import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

/**
 * @author lzy
 */
public interface IOnItemLongClickListener<T> {
    /**
     * common item long click
     * @param view view
     * @param position position
     * @param t Object if exist
     *
     * @return true or false
     * */
    boolean onBackItemLongClicked(@NonNull View view, int position, @Nullable T t);
}
