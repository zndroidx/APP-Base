package com.zndroid.base.callback.inner.mvp;

import com.zndroid.logger.Logger;

public interface ILog {
    String TAG = "^_^";
    /**
     * type error log
     * @param msg String
     * */
    default void logE(String msg) {
        Logger.e(msg);
    }

    /**
     * type warring log
     * @param msg String
     * */
    default void logW(String msg) {
        Logger.w(msg);
    }
    /**
     * type info log
     * @param msg String
     * */
    default void logI(String msg) {
        Logger.i(msg);
    }
    /**
     * type info log
     * @param i int
     * */
    default void logI(int i) {
        Logger.i(String.valueOf(i));
    }
    /**
     * type info log
     * @param b boolean
     * */
    default void logI(boolean b) {
        Logger.i(String.valueOf(b));
    }

    /**
     * all type support log <br>
     * Collections are supported (only available for debug logs)
     *
     * @param o Object
     * */
    default void logD(Object o) {
        Logger.d(o);
    }

    /**
     * String format arguments are supported
     *
     * @param s format string
     * @param values values
     *
     * */
    default void logD(String s, Object ... values) {
        Logger.d(s, values);
    }

    /**
     * type json log
     * @param json json string
     * */
    default void logJson(String json) {
        Logger.json(json);
    }

    /**
     * type xml log
     * @param xml xml string
     * */
    default void logXml(String xml) {
        Logger.xml(xml);
    }
}
