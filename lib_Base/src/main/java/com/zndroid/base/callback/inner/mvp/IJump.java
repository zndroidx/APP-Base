package com.zndroid.base.callback.inner.mvp;

import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

/**
 * @author lzy
 * @date 2021/4/14
 *
 * 页面跳转
 */
@SuppressWarnings("unused")
public interface IJump {
    /**
     * (Activity or Fragment) A goto B Activity
     * @param clazz clz
     * */
    void jumpTo(@NonNull Class<?> clazz);
    /**
     * (Activity or Fragment) A goto B Activity, use intent
     * @param intent intent
     * */
    void jumpTo(@Nullable Intent intent);
    /**
     * (Activity or Fragment) A goto B Activity, use intent with bundle
     * @param intent intent
     * @param bundle bundle
     * */
    void jumpTo(@Nullable Intent intent, @Nullable Bundle bundle);

    /**
     * (Activity or Fragment) A goto B Activity for result, use intent with bundle
     * @param intent intent
     * @param requestCode requestCode
     * */
    void jumpTo(@Nullable Intent intent, int requestCode);

    /**
     * (Activity or Fragment) A goto B Activity for result, use intent with bundle
     * @param intent intent
     * @param bundle bundle
     * @param requestCode requestCode
     * */
    void jumpTo(@Nullable Intent intent, @Nullable Bundle bundle, int requestCode);
}
