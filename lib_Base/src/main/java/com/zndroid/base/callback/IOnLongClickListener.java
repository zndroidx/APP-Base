package com.zndroid.base.callback;

import android.view.View;

import androidx.annotation.NonNull;

/**
 * @author lzy
 */
@SuppressWarnings("unused")
public interface IOnLongClickListener {
    /**
     * common view long click
     * @param view view
     * @return true or false
     * */
    boolean onBackLongClicked(@NonNull View view);
}
