package com.zndroid.base.callback.inner.mvp;

/**
 * @author lzy
 * @date 2021/3/4
 */
public interface IPresenter<V extends IView> {
    /**
     * current page attached
     * @param view v
     * */
    void attachView(V view);
    /**
     * current page detached
     * */
    void detachView();
    /**
     * get current page
     * @return V v
     * */
    V getV();
}
