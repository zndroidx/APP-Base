package com.zndroid.base.callback.inner.mvp;

/**
 * @author lzy
 * @date 2021/3/4
 */
@SuppressWarnings("unused")
public interface IModelFactory {
    /**
     * create model
     * @return IModel model
     * */
    IModel createModel();
}
