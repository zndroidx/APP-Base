package com.zndroid.base.callback;

import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

/**
 * @author lzy
 */
public interface IOnItemClickListener<T> {
    /**
     * common item onclick
     * @param view view
     * @param position position
     * @param t Object if exist
     * */
    void onBackItemClicked(@NonNull View view, int position, @Nullable T t);
}
