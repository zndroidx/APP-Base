package com.zndroid.base.callback.inner.mvp;

/**
 * @author lzy
 *
 * impl it check died or not
 */
public interface IDied {
    /**
     * current 'Activity' / 'Fragment' is died or not
     *
     * @return true or false
     * */
    boolean isDied();
}
