package com.zndroid.base.callback.inner.mvp;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;

/**
 * @author lzy
 * @date 2021/3/4
 */
@SuppressWarnings("unused")
public interface IView extends
        ICommonInteraction,
        IJump,
        IDied,
        ILog 
{
    /**
     * currentRootView by your supply layoutId
     * @return View rootView
     * */
    View currentRootView();

    /**
     * get current Context
     * @return Context current context
     * */
    Context currentContext();

    /**
     * get current Activity
     * @return Context current activity
     * */
    FragmentActivity currentActivity();

    /**
     * Retrieves the intent
     * @return intent Intent
     * */
    Intent currentIntent();

    /**
     * Retrieves a map of extended data from the intent.
     *
     * @return the map of all extras previously added with putExtra(),
     * or null if none have been added. return {@link Fragment#getArguments()} when Fragment
     */
    Bundle getBundleArgs();
}
