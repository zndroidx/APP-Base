package com.zndroid.base.callback.inner.mvp;

import androidx.annotation.DrawableRes;

import com.zndroid.base.widgets.HolderView;

/**
 * General interaction
 *
 * @author lzy
 * @date 2021/3/8
 */
@SuppressWarnings("unused")
public interface ICommonInteraction {
    /**
     * show toast
     * @param msg String
     * */
    void showToast(String msg);

    /**
     * supply instance when show placeholder
     * <code>
     *     <com.zndroid.base.widgets.HolderView
     *         android:layout_width="match_parent"
     *         android:layout_height="wrap_content">
     *
     *         ...
     *         ...other layout
     *         ...
     *
     *     </com.zndroid.base.widgets.HolderView>
     * </code>
     * @return HolderView
     * */
    HolderView onSupplyPlaceholder();
    /**
     * show empty placeholder when repose data is empty, but {@link #onSupplyPlaceholder()} supply at first
     * */
    void showEmptyData();
    /**
     * show error placeholder when repose data is error, but {@link #onSupplyPlaceholder()} supply at first
     * */
    void showErrorData();
    /**
     * show dismiss placeholder when repose data is success or you want dismiss it, but {@link #onSupplyPlaceholder()} supply at first
     * */
    void toDismissPlaceholder();
    /**
     * show net error placeholder when request net is disconnected, but {@link #onSupplyPlaceholder()} supply at first
     * */
    void showNetError();
    /**
     * show loading placeholder when requesting default, but {@link #onSupplyPlaceholder()} supply at first
     * */
    void showLoading();
    /**
     * show loading placeholder when requesting and supply content, but {@link #onSupplyPlaceholder()} supply at first
     * @param msg content
     * */
    void showLoading(String msg);
    /**
     * show loading placeholder when requesting and supply content and can dismiss when back pressed, but {@link #onSupplyPlaceholder()} supply at first
     * @param msg content
     * @param cancelable cancelable
     * */
    void showLoading(String msg, boolean cancelable);
    /**
     * show loading placeholder when requesting and supply content and can dismiss when back pressed, but {@link #onSupplyPlaceholder()} supply at first
     * @param msg content
     * @param resId image res id
     * */
    void showTip(@DrawableRes int resId, String msg);
    /**
     * show loading placeholder when requesting and supply content and can dismiss when back pressed, but {@link #onSupplyPlaceholder()} supply at first
     * @param msg content
     * @param resId image res id
     * @param isShowRetry show retry or not
     * */
    void showTip(@DrawableRes int resId, String msg, boolean isShowRetry);
    /**
     * show 404 placeholder when requested is not found, but {@link #onSupplyPlaceholder()} supply at first
     * */
    void show404();
}
