package com.zndroid.base.callback.inner.mvp;

/**
 * @author lzy
 * @date 2021/3/4
 */
@SuppressWarnings("unused")
public interface IModel<P> {
    interface CallBack<T, F, E> {
        /**
         * request success
         * @param t T
         * */
        void onSuccess(T t);
        /**
         * request failed
         * @param f F
         * */
        void onFailed(F f);
        /**
         * request error
         * @param e e
         * */
        void onError(E e);
        /**
         * request completed
         * */
        void onCompleted();
    }

    /**
     * real request and callback
     * @param params params
     * @param callBack callback
     * */
    void observe(P params, CallBack callBack);
}
