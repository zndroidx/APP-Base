package com.zndroid.base.callback.inner;

/**
 *
 * @author lzy
 * @date 2021/3/25
 */
public interface IOnFragmentVisibilityChangedListener {
    /**
     * fragment visibility listener
     * @param visible true or false
     * */
    void onFragmentVisibilityChanged(boolean visible);
}
