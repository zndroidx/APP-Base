package com.zndroid.base.widgets;

import android.animation.ValueAnimator;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.view.animation.LinearInterpolator;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatTextView;

/**
 * 倒计时控件
 *
 * @author lzy
 * @date 2021/3/15
 */
@SuppressWarnings("unused")
public class CountDownTextView extends AppCompatTextView {
    private final int MAX_ANGLE = 360;

    private int mSweepAngle = 360;
    private ValueAnimator animator;
    private final RectF mRect = new RectF();
    private Paint mBackgroundPaint;
    private ISplashCallBack callBack;

    public ValueAnimator getAnimator() {
        return animator;
    }

    public CountDownTextView(@NonNull Context context) {
        super(context);
        init();
    }

    public CountDownTextView(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public CountDownTextView(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init() {
        mBackgroundPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        // 设置画笔颜色
        mBackgroundPaint.setColor(Color.WHITE);
        // 设置画笔边框宽度
        mBackgroundPaint.setStrokeWidth(dp2px(2));
        // 设置画笔样式为边框类型
        mBackgroundPaint.setStyle(Paint.Style.STROKE);
    }

    public void start(long duration) {
        // 在动画中
        if (mSweepAngle != MAX_ANGLE) {
            return;
        }
        //  初始化属性动画
        animator = ValueAnimator.ofInt(mSweepAngle).setDuration(duration);
        // 设置插值
        animator.setInterpolator(new LinearInterpolator());
        // 设置动画监听
        animator.addUpdateListener(animation -> {
            // 获取属性动画返回的动画值
            mSweepAngle = (int) animation.getAnimatedValue();
            // 重绘自己
            postInvalidate();
            if (mSweepAngle == MAX_ANGLE && (null != callBack)) {
                callBack.onBackSplashComplected();
            }
        });
        // 开始动画
        animator.start();
    }

    @Override
    protected void onDraw(Canvas canvas) {
        int padding = dp2px(4);
        mRect.top = padding;
        mRect.left = padding;
        mRect.right = getWidth() - padding;
        mRect.bottom = getHeight() - padding;

        // 画倒计时线内圆
        //弧线所使用的矩形区域大小
        canvas.drawArc(mRect,
                //开始角度
                -90,
                //扫过的角度
                mSweepAngle,
                //是否使用中心
                false,
                //设置画笔
                mBackgroundPaint);

        super.onDraw(canvas);
    }

    private int dp2px(float dpValue) {
        float density = getContext().getResources().getDisplayMetrics().density;
        return (int)(dpValue * density + 0.5F);
    }

    public void setCallBack(ISplashCallBack callBack) {
        this.callBack = callBack;
    }

    public interface ISplashCallBack {
        /**
         * splash complected
         * */
        void onBackSplashComplected();
    }
}
