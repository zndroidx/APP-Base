package com.zndroid.base;

import android.app.Application;
import android.content.ContentProvider;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.zndroid.base.callback.inner.mvp.ILog;
import com.zndroid.base.ui.atomic.ActLifecycleCallBack;
import com.zndroid.logger.AndroidLogAdapter;
import com.zndroid.logger.FormatStrategy;
import com.zndroid.logger.Logger;
import com.zndroid.logger.PrettyFormatStrategy;
import com.zndroid.utils.XUtils;

/**
 * use ‘ContentProvider’ instead of ‘Application’
 *
 * @author lzy
 */
public class BaseContentProvider extends ContentProvider {
    @Override
    public boolean onCreate() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R) {
            if (requireContext() instanceof Application) {
                initLifecycle((Application) requireContext());
                initUtils(requireContext());
                initLogger();
            }
        } else {
            if (getContext() instanceof Application) {
                initLifecycle((Application) getContext());
                initUtils(getContext());
                initLogger();
            }
        }
        return true;
    }

    private void initLifecycle(Application application) {
        application.registerActivityLifecycleCallbacks(new ActLifecycleCallBack());
    }

    private void initUtils(Context context) {
        XUtils.create().uniformContext(context);
    }

    private void initLogger() {
        Logger.clearLogAdapters();

        FormatStrategy formatStrategy = PrettyFormatStrategy.newBuilder()
                // (Optional) Whether to show thread info or not. Default true
                .showThreadInfo(true)
                // (Optional) How many method line to show. Default 2
                .methodCount(3)
                // (Optional) Skips some method invokes in stack trace. Default 5
                .methodOffset(2)
                // (Optional) Changes the log strategy to print out. Default LogCat
                //.logStrategy(customLog)
                // (Optional) Custom tag for each log. Default 'PRETTY_LOGGER'
                .tag(ILog.TAG)
                .build();

        /* Release and Debug model will be show log at logcat*/
        Logger.addLogAdapter(new AndroidLogAdapter(formatStrategy));
    }

    @Nullable
    @Override
    public Cursor query(@NonNull Uri uri, @Nullable String[] projection, @Nullable String selection, @Nullable String[] selectionArgs, @Nullable String sortOrder) {
        return null;
    }

    @Nullable
    @Override
    public String getType(@NonNull Uri uri) {
        return null;
    }

    @Nullable
    @Override
    public Uri insert(@NonNull Uri uri, @Nullable ContentValues values) {
        return null;
    }

    @Override
    public int delete(@NonNull Uri uri, @Nullable String selection, @Nullable String[] selectionArgs) {
        return 0;
    }

    @Override
    public int update(@NonNull Uri uri, @Nullable ContentValues values, @Nullable String selection, @Nullable String[] selectionArgs) {
        return 0;
    }
}
