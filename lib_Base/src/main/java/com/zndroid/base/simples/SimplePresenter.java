package com.zndroid.base.simples;

import com.zndroid.base.callback.inner.mvp.IView;
import com.zndroid.base.ui.impl.BasePresenter;

/**
 * <b>a simple of {@link com.zndroid.base.ui.impl.BasePresenter} </B><br>
 *
 * 如果您不使用 ‘Presenter’ 的设计思想，但是有些 Activity 又必须传，可以使用该类，防止编译出错
 * @author lzy
 * @date 2021/4/1
 */
public class SimplePresenter extends BasePresenter<IView> {
    //empty for user
}
