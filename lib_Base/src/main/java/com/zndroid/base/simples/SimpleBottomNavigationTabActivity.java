package com.zndroid.base.simples;

import android.view.LayoutInflater;

import androidx.annotation.NonNull;
import androidx.viewpager2.widget.ViewPager2;

import com.zndroid.base.callback.inner.mvp.IView;
import com.zndroid.base.databinding.ZBaseActLayoutSimpleBottomNtbBinding;
import com.zndroid.base.ui.common.CommonNavigationTabActivity;
import com.zndroid.base.ui.impl.BasePresenter;
import com.zndroid.navigation.NavigationTabBarVP2;

/**
 * <b>a simple of bottom NavigationTab activity</b>
 *
 * @author lzy
 */
public abstract class SimpleBottomNavigationTabActivity<P extends BasePresenter<? extends IView>> extends CommonNavigationTabActivity<ZBaseActLayoutSimpleBottomNtbBinding, P> {
    @Override
    protected ViewPager2 onSupplyViewPage2() {
        //findViewById(R.id.base_vp2);
        return currentViewBinding().baseVp2;
    }

    @Override
    protected NavigationTabBarVP2 onSupplyNavigationTabView() {
        //findViewById(R.id.base_ntb2);
        return currentViewBinding().baseNtb2;
    }

    @Override
    protected boolean isBehaviorEnabled() {
        return false;
    }

    @Override
    protected ZBaseActLayoutSimpleBottomNtbBinding onSupplyViewBinding(@NonNull LayoutInflater layoutInflater) {
        //layout is:
        //R.layout.z_base_act_layout_simple_bottom_ntb
        return ZBaseActLayoutSimpleBottomNtbBinding.inflate(layoutInflater);
    }
}
