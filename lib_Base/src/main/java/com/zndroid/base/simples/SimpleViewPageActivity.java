package com.zndroid.base.simples;

import android.view.LayoutInflater;

import androidx.annotation.NonNull;
import androidx.viewpager2.widget.ViewPager2;

import com.zndroid.base.callback.inner.mvp.IView;
import com.zndroid.base.databinding.ZBaseActLayoutSimpleViewPageBinding;
import com.zndroid.base.ui.common.CommonViewPageActivity;
import com.zndroid.base.ui.impl.BasePresenter;

/**
 * <b>a simple of ViewPage activity</b>
 *
 * @author lzy
 * */
public abstract class SimpleViewPageActivity<P extends BasePresenter<? extends IView>> extends CommonViewPageActivity<ZBaseActLayoutSimpleViewPageBinding, P> {
    @Override
    protected ViewPager2 onSupplyViewPage2() {
        //findViewById(R.id.base_vp2);
        return currentViewBinding().baseVp2;
    }

    @Override
    protected ZBaseActLayoutSimpleViewPageBinding onSupplyViewBinding(@NonNull LayoutInflater layoutInflater) {
        //layout is:
        //R.layout.z_base_act_layout_simple_view_page
        return ZBaseActLayoutSimpleViewPageBinding.inflate(layoutInflater);
    }
}
