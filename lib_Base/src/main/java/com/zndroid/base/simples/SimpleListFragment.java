package com.zndroid.base.simples;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.CheckBox;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.zndroid.base.adapter.BaseAdapter;
import com.zndroid.base.annotation.IAdapterType;
import com.zndroid.base.callback.inner.mvp.IView;
import com.zndroid.base.databinding.ZBaseFrgLayoutSimpleListBinding;
import com.zndroid.base.ui.common.CommonListFragment;
import com.zndroid.base.ui.impl.BasePresenter;
import com.zndroid.base.widgets.HolderView;
import com.zndroid.base.widgets.SearchLayout;

import java.util.Collection;
import java.util.List;

/**
 * <b>a simple of list fragment</b>
 *
 * @author lzy
 */
public abstract class SimpleListFragment<P extends BasePresenter<? extends IView>, A extends BaseAdapter<T>, T> extends CommonListFragment<ZBaseFrgLayoutSimpleListBinding, P, A, T> {
    private SearchLayout mSearchMenuLayout;
    private LinearLayout mSelectMenuLayout;
    private View mLineView;
    private CheckBox mCheckBox;

    /**
     * must initialized on {@link #onCreateView(LayoutInflater, ViewGroup, Bundle)}
     *
     * @return SearchLayout
     * */
    protected SearchLayout onSupplySearchLayout() {
        return null;
    }

    /**
     * show default search layout
     *
     * @return true or false
     * */
    protected boolean isSupportSearchMenu() {
        return true;
    }

    /**
     * show default select layout, when {@link #currentAdapter()} instanceof {@link IAdapterType#MULTI}
     *
     * @return true or false
     * */
    protected boolean isSupportSelectMenu() {
        return true;
    }

    /**
     * show default division line layout
     *
     * @return true or false
     * */
    protected boolean isSupportShowLineMenu() {
        return true;
    }

    /**
     * call back when search clicked
     *
     * @param isSearch search clicked is or not
     * @param input text for input
     * */
    protected void onBackToSearch(boolean isSearch, String input) {

    }

    /**
     * call back when search text changed
     *
     * @param text text for input changed
     * */
    protected void onBackSearchTextChanged(String text) {

    }

    @Override
    public HolderView onSupplyPlaceholder() {
        //currentRootView().findViewById(R.id.simple_hv);
        return currentViewBinding().simpleHv;
    }

    @Override
    protected RecyclerView onSupplyRecyclerView() {
        //currentRootView().findViewById(R.id.simple_rv);
        return currentViewBinding().simpleRv;
    }

    @Override
    protected SwipeRefreshLayout onSupplyRefreshLayout() {
        //currentRootView().findViewById(R.id.simple_sw);
        return currentViewBinding().simpleSw;
    }

    @Override
    protected ZBaseFrgLayoutSimpleListBinding onSupplyViewBinding(@NonNull LayoutInflater layoutInflater, @Nullable ViewGroup parent) {
        //layout is:
        //R.layout.z_base_frg_layout_simple_list
        return ZBaseFrgLayoutSimpleListBinding.inflate(layoutInflater, parent, false);
    }

    @Override
    protected void doOnInitView(View currentView) {
        super.doOnInitView(currentView);

        if (null == onSupplySearchLayout()) {
            //currentView.findViewById(R.id.simple_sl_search);
            mSearchMenuLayout = currentViewBinding().simpleSlSearch;
        } else {
            mSearchMenuLayout = onSupplySearchLayout();
        }

        //currentView.findViewById(R.id.simple_ll_operator);
        mSelectMenuLayout = currentViewBinding().simpleLlOperator;
        //currentView.findViewById(R.id.simple_cb_select);
        mCheckBox = currentViewBinding().simpleCbSelect;
        //currentView.findViewById(R.id.simple_view_line);
        mLineView = currentViewBinding().simpleViewLine;
    }

    @Override
    protected void doOnCreate(@Nullable Bundle savedInstanceState) {
        super.doOnCreate(savedInstanceState);

        initLineLayout();
        initSearchLayout();
        initSelectLayout();
    }

    private void initSearchLayout() {
        if (isSupportSearchMenu()) {
            mSearchMenuLayout.setVisibility(View.VISIBLE);
        } else {
            mSearchMenuLayout.setVisibility(View.GONE);
        }

        mSearchMenuLayout.setImeOptions(EditorInfo.IME_ACTION_SEARCH);
        mSearchMenuLayout.setOnSearchListener((str, isSearch) -> onBackToSearch(isSearch, str));

        mSearchMenuLayout.setOnEditorActionListener((v, actionId, event) -> {
            if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                onBackToSearch(true, v.getText().toString());
            }
            return false;
        });

        mSearchMenuLayout.setOnSearchTextChangeListener(this::onBackSearchTextChanged);
    }

    private void initSelectLayout() {
        if (isSupportSelectMenu()
                && (currentAdapter().getAdapterType() == IAdapterType.MULTI)) {
            mSelectMenuLayout.setVisibility(View.VISIBLE);

            if (currentAdapter().getSelectedList().size() != 0
                    && currentAdapter().getData().size() == currentAdapter().getSelectedList().size()) {
                mCheckBox.setChecked(true);
            }

            mCheckBox.setOnCheckedChangeListener((buttonView, isChecked) -> {
                if (buttonView.isPressed() && isChecked) {
                    currentAdapter().toSelectAll();
                } else if (buttonView.isPressed() && !isChecked) {
                    currentAdapter().toUnSelectAll();
                }
            });
        } else {
            mSelectMenuLayout.setVisibility(View.GONE);
        }
    }

    private void initLineLayout() {
        if (isSupportShowLineMenu()) {
            mLineView.setVisibility(View.VISIBLE);
        } else {
            mLineView.setVisibility(View.GONE);
        }
    }

    @Override
    protected void onBackDataChanged(List<T> list) {
        super.onBackDataChanged(list);
        boolean canChecked = isNotEmpty(list)
                && isNotEmpty(currentAdapter().getData())
                && currentAdapter().getData().size() == list.size();
        mCheckBox.setChecked(canChecked);
    }

    private boolean isNotEmpty(Collection<?> collection) {
        return null != collection && !collection.isEmpty();
    }
}
