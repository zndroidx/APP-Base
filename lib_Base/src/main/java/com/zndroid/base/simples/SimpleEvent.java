package com.zndroid.base.simples;

import com.jeremyliao.liveeventbus.core.LiveEvent;

import java.util.List;

/**
 * <b>a simple of 'Live Event bus'</b>
 *
 * @author lzy
 */
@SuppressWarnings("unused")
public class SimpleEvent<T> implements LiveEvent {
    private int type;
    private T data;
    private List<T> list;
    private String extra;

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }

    public List<T> getList() {
        return list;
    }

    public void setList(List<T> list) {
        this.list = list;
    }

    public String getExtra() {
        return extra;
    }

    public void setExtra(String extra) {
        this.extra = extra;
    }
}
