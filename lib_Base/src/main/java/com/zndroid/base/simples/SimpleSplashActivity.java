package com.zndroid.base.simples;

import android.content.Intent;
import android.widget.TextView;

import com.zndroid.base.annotation.ISplashTimerType;
import com.zndroid.base.ui.common.CommonSplashActivity;

/**
 * <b>a simple of splash activity</b>
 *
 * @author lzy
 * @date 2021/4/1
 */
public class SimpleSplashActivity extends CommonSplashActivity<SimplePresenter> {
    @Override
    protected boolean isSupplyWaitNotify() {
        return false;
    }

    @Override
    protected Intent onSupplyMainIntent() {
        return null;
    }

    @Override
    protected long onSupplyDuration() {
        return 4_000;
    }

    @Override
    protected int onSupplyTimerType() {
        return ISplashTimerType.TYPE_CAPSULE;
    }

    @Override
    protected boolean isSupplySkipSplash() {
        boolean isFirstLauncher = true;

        if (isFirstLauncher /** 是否是第一次启动逻辑 */
                    ) {
            return false;
        } else {
            return true;
        }
    }

    @Override
    protected void onUpdateText(TextView textView, long duration) {

    }
}
