package com.zndroid.base.model;

import java.io.Serializable;

/**
 * @author lazy
 * use net transform bean
 * */
@SuppressWarnings("unused")
public interface IBean extends Serializable {
    /**
     * response success or not
     * @return true or false
     * */
    default boolean isSuccess() { return false;}
}
