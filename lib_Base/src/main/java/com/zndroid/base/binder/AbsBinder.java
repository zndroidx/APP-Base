package com.zndroid.base.binder;

import com.chad.library.adapter.base.binder.QuickItemBinder;

/**
 * @author lzy
 * @date 2021/4/14
 */
@SuppressWarnings("unused")
public abstract class AbsBinder<T> extends QuickItemBinder<T> {
    int supportSelectViewId = onSupplySelectId();

    public int getSupportSelectViewId() {
        return supportSelectViewId;
    }

    public void setSupportSelectViewId(int supportSelectViewId) {
        this.supportSelectViewId = supportSelectViewId;
    }

    /**
     * 如果想指定哪个控件可触发选中请复写该方法，默认-1：点击整个item进行选中
     * 注意：需要调用 {@link #addChildClickViewIds(int...)} 将子控件加入可点击列表（否则无法响应点击事件）
     *
     * See Also: <br>
     *     {@link com.zndroid.base.adapter.SingleAdapter}
     *     {@link com.zndroid.base.adapter.MultiAdapter}
     *     {@link #addChildClickViewIds(int...)}
     * */
    public int onSupplySelectId() {
        return -1;
    }
}
