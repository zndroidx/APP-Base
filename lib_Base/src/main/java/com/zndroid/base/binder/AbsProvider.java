package com.zndroid.base.binder;

import androidx.annotation.Nullable;

import com.chad.library.adapter.base.provider.BaseNodeProvider;
import com.zndroid.base.adapter.BaseNodeAdapter;
import com.zndroid.base.exception.SupplyIllegalArgumentException;


/**
 * @author lzy
 * @date 2021/5/20
 */
public abstract class AbsProvider<T extends BaseNodeAdapter> extends BaseNodeProvider {

    @Nullable
    @Override
    public T getAdapter() {
        if (super.getAdapter() instanceof BaseNodeAdapter) {
            return (T) super.getAdapter();
        } else {
            throw new SupplyIllegalArgumentException("adapter", "BaseNodeAdapter");
        }
    }
}
