package com.zndroid.base.exception;

/**
 *
 * @author lzy
 * @date 2021/3/17
 */
public final class SupplyIllegalArgumentException extends IllegalArgumentException {
    public SupplyIllegalArgumentException(String keyWord, String expect) {
        super("your supply '" + keyWord + "' is illegality, need " + expect);
    }
}
