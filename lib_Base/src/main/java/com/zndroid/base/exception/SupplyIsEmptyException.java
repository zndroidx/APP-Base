package com.zndroid.base.exception;

/**
 *
 * @author lzy
 * @date 2021/3/17
 */
public final class SupplyIsEmptyException extends UnsupportedOperationException {
    public SupplyIsEmptyException(String keyWord) {
        super("your supply '" + keyWord + "' is empty, check it pls.");

    }
}
