package com.zndroid.base.exception;

/**
 *
 * @author lzy
 * @date 2021/3/17
 */
public final class SupplyIsNullException extends UnsupportedOperationException {
    public SupplyIsNullException(String keyWord) {
        super("your supply '" + keyWord + "' is null, check it pls.");
    }
}
