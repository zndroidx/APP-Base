package com.zndroid.base.exception;

/**
 * Created by lzy on 2022/5/6.
 */
public final class SupplyIsUnsupportedOperationException extends UnsupportedOperationException {
    public SupplyIsUnsupportedOperationException(String at, String reason) {
        super("logic at " + at + ", because of " + reason);
    }
}
