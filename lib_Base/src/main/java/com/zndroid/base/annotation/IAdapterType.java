package com.zndroid.base.annotation;

import androidx.annotation.IntDef;

import com.zndroid.base.adapter.BaseAdapter;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * common list type for {@link BaseAdapter}
 *
 * @author lazy
 * @date 3/25/21
 */
@IntDef({IAdapterType.DEF, IAdapterType.SINGLE, IAdapterType.MULTI})
@Retention(RetentionPolicy.SOURCE)
public @interface IAdapterType {
    /** 只用做展示 */
    int DEF = 0;
    /** 通用单选列表 */
    int SINGLE = 1;
    /** 通用多选列表 */
    int MULTI = 2;
}
