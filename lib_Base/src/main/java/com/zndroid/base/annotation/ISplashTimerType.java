package com.zndroid.base.annotation;

import androidx.annotation.IntDef;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * ‘跳过’展示类型：无/胶囊状/圆形/圆角矩形
 * @author lzy
 * @date 2021/3/15
 */

@IntDef({ISplashTimerType.TYPE_NONE, ISplashTimerType.TYPE_CAPSULE, ISplashTimerType.TYPE_CIRCLE, ISplashTimerType.TYPE_ROUND_RECT})
@Retention(RetentionPolicy.SOURCE)
public @interface ISplashTimerType {
    int TYPE_NONE = -1;
    int TYPE_CAPSULE = 0;
    int TYPE_CIRCLE = 1;
    int TYPE_ROUND_RECT = 2;
}
