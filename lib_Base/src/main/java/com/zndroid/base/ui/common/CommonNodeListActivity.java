package com.zndroid.base.ui.common;

import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewbinding.ViewBinding;

import com.chad.library.adapter.base.entity.node.BaseNode;
import com.chad.library.adapter.base.provider.BaseNodeProvider;
import com.zndroid.base.adapter.BaseAdapter;
import com.zndroid.base.adapter.BaseNodeAdapter;
import com.zndroid.base.binder.AbsProvider;
import com.zndroid.base.callback.inner.mvp.IView;
import com.zndroid.base.exception.SupplyIsNullException;
import com.zndroid.base.ui.impl.BasePresenter;

/**
 * @author lzy
 * @date 2021/5/19
 */
@SuppressWarnings("unused")
public abstract class CommonNodeListActivity <VB extends ViewBinding, P extends BasePresenter<? extends IView>, A extends BaseNodeAdapter> extends CommonActivity<VB, P> {
    private @Nullable A adapter;
    private @Nullable RecyclerView recyclerView;

    /**
     * must supply Adapter instances of {@link BaseAdapter}
     * @return A a adapter
     * */
    protected abstract A onSupplyAdapter();

    /**
     * must supply RecyclerView instances of {@link RecyclerView}
     * @return RecyclerView
     * */
    protected abstract RecyclerView onSupplyRecyclerView();

    /**
     * must supply Providers instances of {@link BaseNodeProvider}
     * @return BaseNodeProvider[]
     * */
    protected AbsProvider<A>[] onSupplyProviders() {
        return null;
    }

    /**
     * default {@link LinearLayoutManager}
     * @return RecyclerView.LayoutManager
     * */
    protected RecyclerView.LayoutManager onSupplyLayoutManager() {
        return new LinearLayoutManager(getApplicationContext());
    }

    /**
     * default {@link RecyclerView.ItemDecoration}
     * @return RecyclerView.ItemDecoration
     * */
    protected RecyclerView.ItemDecoration onSupplyItemDecoration() {
        return new DividerItemDecoration(getApplicationContext(), DividerItemDecoration.VERTICAL);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        initAdapter();
        super.onCreate(savedInstanceState);

        initRecyclerView();
    }

    private void initAdapter() {
        adapter = onSupplyAdapter();
        if (null != adapter) {
            boolean supplyProvidersNotEmpty = (null != onSupplyProviders()) && (onSupplyProviders().length > 0);
            if (supplyProvidersNotEmpty) {
                for (BaseNodeProvider it : onSupplyProviders()) {
                    adapter.addNodeProvider(it);
                }
            }

            adapter.setItemClickListener((view, position, t) -> onBackItemClicked(position, t));
            adapter.setItemLongClickListener((view, position, t) -> onBackItemLongClicked(position, t));
        } else {
            throw new SupplyIsNullException("adapter");
        }
    }

    private void initRecyclerView() {
        recyclerView = onSupplyRecyclerView();
        if (null != recyclerView) {
            recyclerView.setLayoutManager(onSupplyLayoutManager());
            recyclerView.setAdapter(adapter);
            if (null != onSupplyItemDecoration()) {
                recyclerView.addItemDecoration(onSupplyItemDecoration());
            }
        } else {
            throw new SupplyIsNullException("recyclerView");
        }
    }

    public A currentAdapter() {
        return adapter;
    }

    /**
     * 如果不想在 provider 里处理点击回调可以在该回调方法中处理
     * */
    protected void onBackItemClicked(int position, @Nullable BaseNode t) {

    }

    /**
     * 如果不想在 provider 里处理长按回调可以在该回调方法中处理
     * */
    protected boolean onBackItemLongClicked(int position, @Nullable BaseNode t) {
        return false;
    }
}
