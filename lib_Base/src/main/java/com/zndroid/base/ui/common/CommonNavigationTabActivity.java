package com.zndroid.base.ui.common;

import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.viewbinding.ViewBinding;
import androidx.viewpager2.widget.ViewPager2;

import com.zndroid.base.callback.inner.mvp.IView;
import com.zndroid.base.exception.SupplyIllegalArgumentException;
import com.zndroid.base.exception.SupplyIsNullException;
import com.zndroid.base.ui.impl.BasePresenter;
import com.zndroid.navigation.NavigationTabBarVP2;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * Common NavigationTabActivity , it can used by main tab
 * @author lzy
 */
@SuppressWarnings("unused")
public abstract class CommonNavigationTabActivity<VB extends ViewBinding, P extends BasePresenter<? extends IView>> extends CommonViewPageActivity<VB, P> {
    private @Nullable NavigationTabBarVP2 navigationTabBar;

    /**
     * supply {@link com.zndroid.navigation.NavigationTabBarVP2} as impl, and use {@link #onSupplyViewPage2()} impl.
     * like this:
     * <code>
     *     <com.zndroid.navigation.NavigationTabBarVP2
     *         android:id="@+id/base_ntb2"
     *         android:layout_width="match_parent"
     *         android:layout_height="60dp"
     *         app:ntb_corners_radius="0dp"
     *
     *         app:ntb_badged="true"
     *         app:ntb_badge_size="10sp"
     *         app:ntb_badge_bg_color="#ffff0000"
     *         app:ntb_badge_title_color="#ffffffff"
     *         app:ntb_badge_gravity="top"
     *         app:ntb_badge_position="right"
     *         app:ntb_badge_use_typeface="true"
     *
     *         app:ntb_titled="true"
     *         app:ntb_title_size="12sp"
     *         app:ntb_title_mode="all"
     *
     *         app:ntb_scaled="false"
     *         app:ntb_tinted="true"
     *         app:ntb_swiped="false"
     *         app:ntb_lined="true"
     *         app:ntb_line_color="#c4c4c4"
     *         app:ntb_line_width="1"
     *
     *         app:ntb_animation_duration="200"
     *         app:ntb_typeface="fonts/agency.ttf"
     *
     *         app:ntb_bg_color="#F7F7F7"
     *         app:ntb_active_color="@color/purple_500"
     *         app:ntb_inactive_color="#BFBFBF"
     *         />
     * </code>
     *
     * @return instance of {@link com.zndroid.navigation.NavigationTabBarVP2}
     * */
    protected abstract NavigationTabBarVP2 onSupplyNavigationTabView();

    /**
     * supply tab bars instance
     *
     * @return NavigationTabBarVP2.Model[]
     * */
    protected abstract NavigationTabBarVP2.Model[] onSupplyBars();

    /**
     * Override to 'true' if you want use at {@link androidx.coordinatorlayout.widget.CoordinatorLayout}
     *
     * @return true or false
     * */
    protected abstract boolean isBehaviorEnabled();

    /**
     * get {@link NavigationTabBarVP2} instance
     *
     * @return NavigationTabBarVP2
     * */
    @Nullable
    public NavigationTabBarVP2 currentNavigationTabBar() {
        return navigationTabBar;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        initNavigationTab();
    }

    private void initNavigationTab() {
        if (checkParamIsOk()) {
            initBar();
        }
    }

    private boolean checkParamIsOk() {
        if (null == onSupplyNavigationTabView()) {
            throw new SupplyIsNullException("NavigationTabBarVP2（#onSupplyNavigationTabView(...)）");
        }

        if (null == onSupplyBars() || 0 == onSupplyBars().length) {
            throw new SupplyIllegalArgumentException("SupplyBars[#onSupplyBars(...)]", "not null or empty.");
        }

        return true;
    }

    private void initBar() {
        navigationTabBar = onSupplyNavigationTabView();

        //bind viewpage2
        navigationTabBar.setViewPager(onSupplyViewPage2(), onSupplyDefaultIndex());

        //bind bars
        final ArrayList<NavigationTabBarVP2.Model> models = new ArrayList<>(Arrays.asList(onSupplyBars()));
        navigationTabBar.setModels(models);

        //IMPORTANT: enable scroll behaviour in coordinator layout
        navigationTabBar.setBehaviorEnabled(isBehaviorEnabled());

        //listener
        navigationTabBar.setOnPageChangeListener(new ViewPager2.OnPageChangeCallback() {
            @Override
            public void onPageSelected(int position) {
                onBackPageSelected(position, navigationTabBar.getModels().get(position));
            }
        });
    }

    /**
     * page selected call back
     *
     * @param position index
     * @param model current bar model
     * */
    protected void onBackPageSelected(int position, NavigationTabBarVP2.Model model) {

    }
}
