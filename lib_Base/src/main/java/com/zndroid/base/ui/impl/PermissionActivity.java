package com.zndroid.base.ui.impl;

import android.os.Bundle;

import androidx.annotation.Nullable;

import com.zndroid.base.ui.atomic.AtomicActivity;
import com.zndroid.permission.OnPermissionCallback;
import com.zndroid.permission.PermissionX;

import java.util.List;
import java.util.Objects;

/**
 * @author lzy
 * 权限请求基类 Activity，如果包含Fragment权限，只需要在对应Fragment中处理即可，没必要同时实现，根据单一原则，建议在Fragment中处理权限操作
 * */
public abstract class PermissionActivity extends AtomicActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (null != onSupplyPermissions() && Objects.requireNonNull(onSupplyPermissions()).size() > 0) {
            PermissionX
                    .with(this)
                    .permission(onSupplyPermissions())
                    .request(new OnPermissionCallback() {
                        @Override
                        public void onGranted(List<String> list, boolean b) {
                            if (b) {
                                onBackAllPermissionGranted();
                            }
                        }

                        @Override
                        public void onDenied(List<String> permissions, boolean never) {
                            if (!never) {/*禁止后不再提示*/
                                onBackPermissionDenied(permissions);
                            }
                        }
                    });
        }
    }

    /**
     * 子类如果有需要请求权限的话，必须要复写该方法
     * @return need requesting permissions
     * */
    protected @Nullable abstract List<String> onSupplyPermissions();
    /**
     * 权限（所有）请求成功回调，如果子类需要处理后续操作请复写该方法
     * */
    protected abstract void onBackAllPermissionGranted();
    /**
     * 权限被拒绝回调，如果子类需要处理后续操作请复写该方法
     * @param deniedList List
     * */
    protected abstract void onBackPermissionDenied(List<String> deniedList);
}
