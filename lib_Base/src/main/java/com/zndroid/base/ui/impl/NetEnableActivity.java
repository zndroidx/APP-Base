package com.zndroid.base.ui.impl;

import android.util.Log;

import androidx.annotation.Nullable;

import com.zndroid.base.callback.inner.mvp.ILog;
import com.zndroid.netmonitor.NetType;
import com.zndroid.netmonitor.NetworkMonitor;
import com.zndroid.netmonitor.monitor.INetChangeCallBack;
import com.zndroid.permission.Permissions;

import java.util.List;
import java.util.Objects;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.atomic.AtomicBoolean;

/**
 * @author lzy
 * 提供网络监控的基类 Activity
 * */
@SuppressWarnings("unused")
public abstract class NetEnableActivity extends PermissionActivity implements INetChangeCallBack {
    private final AtomicBoolean isRequestedPermission = new AtomicBoolean(false);

    @Override
    protected @Nullable List<String> onSupplyPermissions() {
        List<String> permissions = new CopyOnWriteArrayList<>();

        if (isSupportNetworkMonitor()) {
            //权限明细请参考
            //https://developer.android.google.cn/reference/android/Manifest.permission?hl=zh_cn
            permissions.add(Permissions.READ_PHONE_STATE);
        }

        return permissions;
    }

    @Override
    protected void onResume() {
        super.onResume();

        if (isSupportNetworkMonitor()) {
            if (isEmptyPermissions()) {
                Log.e(ILog.TAG, "NetworkMonitor need permissions must be granted if you instanceof NetEnableActivity");
                return;
            }

            if (isRequestedPermission.get()) {
                NetworkMonitor.getInstance().startMonitor(this, this);
            }
        }
    }

    @Override
    protected void onPause() {
        super.onPause();

        if (isSupportNetworkMonitor() && isRequestedPermission.getAndSet(false)) {
            NetworkMonitor.getInstance().stopMonitor();
        }
    }

    /**
     * check has net
     * @return true or false
     * */
    protected boolean isNetConnected() {
        if (isSupportNetworkMonitor()) {
            if (isEmptyPermissions()) {
                Log.e(ILog.TAG, "NetworkMonitor need permissions must be granted if you instanceof NetEnableActivity");
                return false;
            }
            return NetworkMonitor.getInstance().isNetConnected(this);
        }
        else {
            Log.e(ILog.TAG, "override #isSupportNetworkMonitor() if you want use NetworkMonitor");
            return false;
        }

    }

    @Override
    protected void onBackAllPermissionGranted() {
        isRequestedPermission.getAndSet(true);
    }

    @Override
    protected void onBackPermissionDenied(List<String> deniedList) {
        Log.e(ILog.TAG, "some permission of NetworkMonitor denied by user");
        isRequestedPermission.set(false);
    }

    private boolean isEmptyPermissions() {
        return onSupplyPermissions() == null || Objects.requireNonNull(onSupplyPermissions()).size() <= 0;
    }

    @Override
    public void onNetChanged(boolean isConnect, @NetType String type, int signalLevel) {

    }

    @Override
    public void onNetConnected(boolean isConnect, @NetType String type) {

    }

    /**
     * It is up to the subclasses to enable or disable the 'NetworkMonitor' function
     * @return true is open monitor
     * */
    protected abstract boolean isSupportNetworkMonitor();

}
