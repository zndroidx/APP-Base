package com.zndroid.base.ui.impl;

import java.util.List;

/**
 * Fragment not implement NetworkMonitor，use {@link NetEnableActivity} or child implement NetworkMonitor.
 *
 * @author lazy
 * @date 3/23/21
 */
public class NetEnableFragment extends PermissionFragment {
    @Override
    protected List<String> onSupplyPermissions() {
        return null;
    }

    @Override
    protected void onBackAllPermissionGranted() {

    }

    @Override
    protected void onBackPermissionDenied(List<String> deniedList) {

    }
}
