package com.zndroid.base.ui.atomic;

import android.content.Intent;
import android.os.Bundle;

import androidx.activity.result.ActivityResultCallback;
import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContract;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityOptionsCompat;
import androidx.fragment.app.Fragment;

/**
 * @author lzy
 * Kernel Fragment,other Fragment generate from this
 */
@SuppressWarnings("unused")
public abstract class AtomicFragment extends Fragment {
    protected ActivityResultLauncher<Intent> defaultLauncher;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        defaultLauncher = registerForActivityResult(new ActivityResultContracts.StartActivityForResult(), result -> {
            //nothing
        });
    }

    /**
     * Hi Hi, for new API
     * Jump other Activity you can keep use {@link #startActivity(Intent)} or {@link #startActivity(Intent, Bundle)} because of simpleness.
     * Strongly recommend use it when jump other Activity with result.
     *
     * Get 'launcher' by {@link #registerForActivityResult(ActivityResultContract, ActivityResultCallback)} and LifecycleOwners must call register before they are STARTED
     *
     * @param launcher ActivityResultLauncher
     * @param input input
     * */
    protected <I> void toActivityCallBackAble(@NonNull ActivityResultLauncher<I> launcher, I input) {
        launcher.launch(input);
    }

    /**
     * like {@link #toActivityCallBackAble(ActivityResultLauncher, Object)} and customized.
     * @param launcher ActivityResultLauncher
     * @param options ActivityOptionsCompat
     * @param input input
     * */
    protected <I> void toActivityCallBackAble(@NonNull ActivityResultLauncher<I> launcher, @Nullable ActivityOptionsCompat options, I input) {
        launcher.launch(input, options);
    }

    /**
     * default jump to other Intent
     *
     * @param intent intent will be jump
     * */
    protected void toActivityCallBackAble(Intent intent) {
        defaultLauncher.launch(intent);
    }
}
