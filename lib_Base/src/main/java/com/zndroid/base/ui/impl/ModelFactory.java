package com.zndroid.base.ui.impl;

import com.zndroid.base.callback.inner.mvp.IModel;
import com.zndroid.base.callback.inner.mvp.IModelFactory;

/**
 * @author lzy
 * @date 2021/3/4
 */
public abstract class ModelFactory<M extends IModel> implements IModelFactory {
    /**
     * create real model to request
     * @return M model
     * */
    @Override
    public abstract M createModel();
}
