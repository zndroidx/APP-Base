package com.zndroid.base.ui.common;

import android.os.Bundle;
import android.view.View;

import androidx.annotation.Nullable;
import androidx.viewbinding.ViewBinding;

import com.zndroid.base.callback.inner.mvp.IView;
import com.zndroid.base.ui.impl.BaseActivity;
import com.zndroid.base.ui.impl.BasePresenter;
import com.zndroid.widget.title.TitleBar;

/**
 * @author lazy
 * @date 2021/3/17
 */
public abstract class CommonActivity<VB extends ViewBinding, P extends BasePresenter<? extends IView>> extends BaseActivity<VB, P> {
    /**
     * current activity show 'TitleBar' when supply not 'null'.
     * @return TitleBar
     * */
    protected TitleBar onSupplyTitleBar() {
        return null;
    }

    /**
     * callback when click TitleBar
     * */
    protected void onBackClickTitle() {

    }

    /**
     * callback when click TitleBar left party, 'finish()' will called default.
     * */
    protected void onBackClickTitleLeft() {
        finish();
    }

    /**
     * callback when click TitleBar right party
     * */
    protected void onBackClickTitleRight() {

    }

    @Override
    protected void doOnCreate(@Nullable Bundle savedInstanceState) {
        super.doOnCreate(savedInstanceState);

        //about title bar
        TitleBar titleBar = onSupplyTitleBar();

        if (null != titleBar) {
            titleBar.setVisibility(View.VISIBLE);
            titleBar.setOnClickListener((v)-> onBackClickTitle());
            titleBar.getLeftView().setOnClickListener((v) -> onBackClickTitleLeft());
            titleBar.getRightView().setOnClickListener((v) -> onBackClickTitleRight());
        }
    }
}
