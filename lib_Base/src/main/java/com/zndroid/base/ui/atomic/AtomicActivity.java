package com.zndroid.base.ui.atomic;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;

import androidx.activity.result.ActivityResultCallback;
import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContract;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityOptionsCompat;

import com.zndroid.base.callback.inner.mvp.ILog;

/**
 * @author lzy
 * Kernel Activity,other Activity generate from this
 */
@SuppressWarnings("unused")
public abstract class AtomicActivity extends AppCompatActivity {
    protected ActivityResultLauncher<Intent> defaultLauncher;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        defaultLauncher = registerForActivityResult(new ActivityResultContracts.StartActivityForResult(), result -> {
            //nothing
        });
    }

    /**
     * check OS can resolve the specified intent action
     * if SDK API >= 30(Android Q) you must code like this on your 'AndroidManifest.xml'
     * <Code>
     *     <manifest xmlns:android="http://schemas.android.com/apk/res/android"
     *          package="com.app.base">
     *
     *          <application
     *              ...
     *          </application>
     *
     *          <queries>
     *              <!-- 就像这样（打开相册示例） -->
     *              <intent>
     *                  <action android:name="android.intent.action.PICK" />
     *                  <data android:mimeType="image/*" />
     *              </intent>
     *          </queries>
     *
     *      </manifest>
     * </Code>
     * */
    @SuppressLint("QueryPermissionsNeeded")
    protected boolean isResolveIntent(@NonNull Intent intent) {
        boolean canResolve = (null != intent.resolveActivity(getPackageManager()));
        //for tip
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R && !canResolve) {
            Log.w(ILog.TAG, "check query permission【<queries>...</queries>】 on your 'AndroidManifest.xml' when Android API >= 30 .");
        }
        return canResolve;
    }

    /**
     * Hi Hi, for new API
     * Jump other Activity you can keep use {@link #startActivity(Intent)} or {@link #startActivity(Intent, Bundle)} because of simpleness.
     * Strongly recommend use it when jump other Activity with result.
     *
     * Get 'launcher' by {@link #registerForActivityResult(ActivityResultContract, ActivityResultCallback)} and LifecycleOwners must call register before they are STARTED
     *
     * @param launcher ActivityResultLauncher
     * @param input input
     * */
    protected <I> void toActivityCallBackAble(@NonNull ActivityResultLauncher<I> launcher, I input) {
        launcher.launch(input);
    }

    /**
     * like {@link #toActivityCallBackAble(ActivityResultLauncher, Object)} and customized.
     * @param launcher ActivityResultLauncher
     * @param options ActivityOptionsCompat
     * @param input input
     * */
    protected <I> void toActivityCallBackAble(@NonNull ActivityResultLauncher<I> launcher, @Nullable ActivityOptionsCompat options, I input) {
        launcher.launch(input, options);
    }

    /**
     * default jump to other Intent
     *
     * @param intent intent will be jump
     * */
    protected void toActivityCallBackAble(Intent intent) {
        defaultLauncher.launch(intent);
    }
}
