### 复写方法规则(包含接口)
1. 需要获取参数的
onSupply_xxx other type
is_xxx  boolean type
2. 执行后返回数据的(回调)
onBack_xxx
3. 执行流程
前：doPre_xxx
中：do_xxx
后: did_xxx
4. 调用方法
to_xxx
show_xxx